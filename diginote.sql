-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 22 Juillet 2017 à 22:21
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `diginote`
--

-- --------------------------------------------------------

--
-- Structure de la table `bulletins`
--

CREATE TABLE `bulletins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `classe_id` int(10) UNSIGNED NOT NULL,
  `annee` int(10) NOT NULL,
  `moyenne` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bulletins`
--

INSERT INTO `bulletins` (`id`, `user_id`, `classe_id`, `annee`, `moyenne`, `created_at`, `updated_at`) VALUES
(1, 46, 1, 2012, NULL, '2017-07-11 17:01:51', '2017-07-11 17:01:51'),
(2, 46, 2, 2013, NULL, '2017-07-11 17:02:07', '2017-07-11 17:02:07'),
(3, 46, 3, 2014, NULL, '2017-07-11 18:24:07', '2017-07-11 18:24:07'),
(4, 45, 1, 2015, NULL, '2017-07-12 18:43:25', '2017-07-12 18:43:25'),
(5, 8, 3, 2017, NULL, '2017-07-19 21:36:01', '2017-07-19 21:36:01'),
(6, 8, 2, 2016, NULL, '2017-07-19 21:39:03', '2017-07-19 21:39:03'),
(7, 19, 1, 2017, NULL, '2017-07-21 07:55:46', '2017-07-21 07:55:46'),
(8, 45, 2, 2016, NULL, '2017-07-22 01:19:24', '2017-07-22 01:19:24'),
(9, 21, 2, 2017, NULL, '2017-07-23 00:44:45', '2017-07-23 00:44:45'),
(10, 44, 3, 2017, NULL, '2017-07-23 00:50:50', '2017-07-23 00:50:50'),
(11, 13, 1, 2016, NULL, '2017-07-23 01:12:18', '2017-07-23 01:12:18'),
(12, 22, 1, 2016, NULL, '2017-07-23 01:36:59', '2017-07-23 01:36:59');

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capacite` int(11) NOT NULL,
  `frais_scolaire` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `classes`
--

INSERT INTO `classes` (`id`, `code`, `nom`, `capacite`, `frais_scolaire`, `created_at`, `updated_at`) VALUES
(1, '2CL', 'Seconde A2', 60, 2500, '2017-07-03 04:00:00', '2017-07-03 04:00:00'),
(2, '1CL', 'Première A1', 50, 3000, '2017-07-03 04:00:00', '2017-07-03 04:00:00'),
(3, 'TLE', 'Terminale', 80, 3500, '2017-07-11 18:22:06', '2017-07-11 18:22:06');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `discussion_id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `discussion_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 34, 1, 'Bravo! Vous avez très bien travaillé pour cet examen, vous êtes un génie!', '2017-07-20 04:00:00', '2017-07-20 04:00:00'),
(2, 8, 1, 'Merci Mr le professeur!', '2017-07-20 04:00:00', '2017-07-20 04:00:00'),
(3, 34, 6, 'Votre note est prête!', '2017-07-20 23:53:54', '2017-07-20 23:53:54'),
(4, 46, 6, 'Merci!', '2017-07-20 23:56:55', '2017-07-20 23:56:55'),
(5, 46, 4, 'Très bon examen!', '2017-07-20 23:59:02', '2017-07-20 23:59:02'),
(6, 46, 4, 'Rdv au final ;)', '2017-07-20 23:59:24', '2017-07-20 23:59:24'),
(7, 34, 4, 'Parfait', '2017-07-21 00:00:16', '2017-07-21 00:00:16'),
(8, 34, 5, 'Bonjour Black Meduse, votre note est prête!', '2017-07-21 07:40:05', '2017-07-21 07:40:05'),
(9, 46, 5, 'Merci, je viens de voir.', '2017-07-21 07:41:23', '2017-07-21 07:41:23'),
(10, 34, 9, 'Très bien mon grand', '2017-07-21 17:41:15', '2017-07-21 17:41:15'),
(11, 38, 6, 'Bravo mon enfant!', '2017-07-21 21:21:42', '2017-07-21 21:21:42'),
(12, 32, 10, 'Bravo! Blacky, ta note est prête ;)', '2017-07-21 21:38:11', '2017-07-21 21:38:11'),
(13, 46, 10, 'Je ferais une réclamation, ma notes a été mal calculée.', '2017-07-21 21:42:48', '2017-07-21 21:42:48'),
(14, 38, 10, 'Bravo mon fils!', '2017-07-21 21:44:08', '2017-07-21 21:44:08'),
(15, 30, 12, 'Bravo Trudeau Simon!', '2017-07-22 01:26:15', '2017-07-22 01:26:15'),
(16, 45, 12, 'Merci!', '2017-07-22 01:27:17', '2017-07-22 01:27:17'),
(17, 34, 16, 'Votre note est prête Mr Simon, vous vous êtes bien débrouillé', '2017-07-23 00:25:27', '2017-07-23 00:25:27'),
(18, 34, 15, 'Vous avez une note de 75/100, vous pouvez encore mieux faire', '2017-07-23 00:25:58', '2017-07-23 00:25:58'),
(19, 34, 14, 'Pas mal Simon vous avez le moov!', '2017-07-23 00:26:16', '2017-07-23 00:26:16'),
(20, 34, 17, 'Good job!', '2017-07-23 00:27:38', '2017-07-23 00:27:38'),
(21, 34, 18, 'Pas mal, Faites moi savoir si vous avez besoin d\'aide.', '2017-07-23 00:28:07', '2017-07-23 00:28:07'),
(22, 45, 18, 'Merci Miranda, je viendrai vous voir après le cours de la semaine prochaine.', '2017-07-23 00:33:00', '2017-07-23 00:33:00'),
(23, 45, 17, 'Thank you!', '2017-07-23 00:33:16', '2017-07-23 00:33:16'),
(24, 45, 16, 'Merci, j\'étais presque à 100%', '2017-07-23 00:33:36', '2017-07-23 00:33:36'),
(25, 45, 15, 'Oui, je m\'applique, Merci, Mon père viendra surement commenter cela ;)', '2017-07-23 00:34:10', '2017-07-23 00:34:10'),
(26, 45, 14, 'Merci, le cour sur les plantes carnivores était vraiment intéressant, vous êtes une bonne enseignantes! ;)', '2017-07-23 00:35:23', '2017-07-23 00:35:23'),
(27, 41, 9, 'Bravos mon fils, tu es le meilleur!', '2017-07-23 00:38:00', '2017-07-23 00:38:00'),
(28, 41, 8, 'Très bonne note mon fiston!', '2017-07-23 00:38:14', '2017-07-23 00:38:14'),
(29, 41, 18, 'Applique toi fiston, les maths, c\'est pas sorcier', '2017-07-23 00:39:02', '2017-07-23 00:39:02'),
(30, 41, 16, 'Yes!', '2017-07-23 00:39:09', '2017-07-23 00:39:09'),
(31, 41, 14, 'Arrête de draguer ta prof fiston', '2017-07-23 00:39:35', '2017-07-23 00:39:35'),
(32, 19, 9, 'Merci Papa, je suis tes traces!', '2017-07-23 00:40:49', '2017-07-23 00:40:49'),
(33, 19, 8, 'Merci papa, je suis un bon élève!', '2017-07-23 00:41:05', '2017-07-23 00:41:05'),
(34, 19, 9, 'Je serais un peu en retard ce soir.. je vais étudier avec des amis', '2017-07-23 00:41:37', '2017-07-23 00:41:37'),
(35, 30, 19, 'Distrait, sois plus attentif', '2017-07-23 01:16:31', '2017-07-23 01:16:31'),
(36, 30, 21, 'Excellent, maintenant faudra garder cette place', '2017-07-23 01:17:42', '2017-07-23 01:17:42'),
(37, 30, 23, 'Bon travail', '2017-07-23 01:20:05', '2017-07-23 01:20:05'),
(38, 48, 21, 'Bravo mon fils', '2017-07-23 01:23:43', '2017-07-23 01:23:43');

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE `cours` (
  `id` int(10) UNSIGNED NOT NULL,
  `classe_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`id`, `classe_id`, `code`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'MTH1000', 'Mathématiques', 'Cours portant sur l\'apprentissage des notions mathématiques fondamentales ou élémentaires de base. Notions de cours : Fonctions polynômes, géométrie, statistiques', '2017-07-04 04:00:00', '2017-07-23 00:04:30'),
(6, 2, 'MTH2000', 'Mathématiques', 'Cours portant sur l\'apprentissage des notions mathématiques fondamentales ou élémentaires de base. Notions de cours : Étude de fonctions, équations différentielles, probabilité', '2017-07-23 00:03:14', '2017-07-23 00:03:14'),
(2, 2, 'BIO2024', 'Biologie', 'Cours portant sur les sciences de la nature et de l\'histoire naturelle des êtres vivants.', '2017-07-04 04:00:00', '2017-07-23 00:08:06'),
(8, 3, 'BIO3024', 'Biologie', 'Cours portant sur les sciences de la nature et de la génétique, reproduction des êtres vivants', '2017-07-23 00:09:34', '2017-07-23 00:09:34'),
(3, 1, 'PHY1020', 'Physiques', 'Cours portant sur l\'explication des concepts physiques, les lois et les théories. Notions de cours : optique, circuits électriques, les forces', '2017-07-21 22:29:52', '2017-07-23 00:17:14'),
(4, 2, 'ALG1520', 'Algèbre', 'Cours portant sur  l\'étude des structures algébriques. Notions de cours : arithmétique généralisée, opérations', '2017-07-21 22:31:44', '2017-07-23 00:29:51'),
(5, 3, 'PRO3350', 'Programmation', 'Cours portant sur l\'apprentissage des différentes solutions de développement logiciels', '2017-07-21 22:32:19', '2017-07-23 00:32:24'),
(7, 3, 'MTH3000', 'Mathématiques', 'Cours portant sur l\'apprentissage des notions mathématiques fondamentales ou élémentaires de base. Notions de cours : Variations de fonctions, Calcul intégral, Suites numériques', '2017-07-23 00:07:00', '2017-07-23 00:07:00'),
(9, 1, 'BIO1024', 'Biologie', 'Cours portant sur la science du vivant', '2017-07-23 00:10:20', '2017-07-23 00:10:20'),
(10, 2, 'PHY2020', 'Physiques', 'Cours portant sur l\'explication des concepts physiques, les lois et les théories. Notions de cours : thermodynamique, électromagnétisme', '2017-07-23 00:18:55', '2017-07-23 00:18:55'),
(11, 3, 'PHY3020', 'Physiques', 'Cours portant sur l\'explication des concepts physiques, les lois et les théories. Notions de cours : mécanique newtonienne, relativité, électronique', '2017-07-23 00:27:13', '2017-07-23 00:27:13'),
(12, 3, 'ALG2520', 'Algèbre', 'Cours portant sur  l\'étude des structures algébriques. Notions de cours : équations, pôlynomes, structure algébriques', '2017-07-23 00:30:42', '2017-07-23 00:30:42'),
(13, 2, 'PRO2350', 'Programmation', 'Cours portant sur la logique, les algorithmes et les pseudocodes en programmation', '2017-07-23 00:34:09', '2017-07-23 00:34:09'),
(14, 1, 'ECV1000', 'Éducation civique', 'Cours portant sur la citoyenneté et les valeurs qu\'un système éducatif et culturel veut diffuser.', '2017-07-23 00:36:03', '2017-07-23 00:51:36'),
(15, 2, 'ECV2000', 'Éducation civique', 'Cours portant sur la citoyenneté et les valeurs qu\'un système éducatif et culturel veut diffuser.', '2017-07-23 00:36:26', '2017-07-23 00:51:44');

-- --------------------------------------------------------

--
-- Structure de la table `discussions`
--

CREATE TABLE `discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `discussions`
--

INSERT INTO `discussions` (`id`, `user_id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 34, 'CL2CR1', '2017-07-20 03:20:57', '2017-07-20 03:20:57'),
(2, 34, 'CL2CR1', '2017-07-20 03:21:13', '2017-07-20 03:21:13'),
(3, 34, 'CL2CR1', '2017-07-20 03:23:12', '2017-07-20 03:23:12'),
(4, 34, 'CL2CR1', '2017-07-20 03:23:41', '2017-07-20 03:23:41'),
(5, 34, 'CL2CR1', '2017-07-20 03:24:33', '2017-07-20 03:24:33'),
(6, 34, 'CL2CR1', '2017-07-20 03:24:53', '2017-07-20 03:24:53'),
(7, 34, 'CL1CR1', '2017-07-21 07:56:16', '2017-07-21 07:56:16'),
(8, 34, 'CL1CR1', '2017-07-21 17:39:51', '2017-07-21 17:39:51'),
(9, 34, 'CL1CR1', '2017-07-21 17:40:37', '2017-07-21 17:40:37'),
(10, 32, 'CL3CR2', '2017-07-21 21:37:34', '2017-07-21 21:37:34'),
(11, 30, 'CL2CR1', '2017-07-22 01:25:43', '2017-07-22 01:25:43'),
(12, 30, 'CL2CR1', '2017-07-22 01:25:54', '2017-07-22 01:25:54'),
(13, 34, 'CL1CR2', '2017-07-23 00:24:01', '2017-07-23 00:24:01'),
(14, 34, 'CL1CR2', '2017-07-23 00:24:21', '2017-07-23 00:24:21'),
(15, 34, 'CL1CR2', '2017-07-23 00:24:37', '2017-07-23 00:24:37'),
(16, 34, 'CL1CR2', '2017-07-23 00:24:49', '2017-07-23 00:24:49'),
(17, 34, 'CL1CR1', '2017-07-23 00:27:20', '2017-07-23 00:27:20'),
(18, 34, 'CL1CR1', '2017-07-23 00:27:27', '2017-07-23 00:27:27'),
(19, 30, 'CL2CR3', '2017-07-23 01:15:59', '2017-07-23 01:15:59'),
(20, 30, 'CL2CR3', '2017-07-23 01:16:58', '2017-07-23 01:16:58'),
(21, 30, 'CL2CR3', '2017-07-23 01:17:13', '2017-07-23 01:17:13'),
(22, 30, 'CL2CR3', '2017-07-23 01:18:29', '2017-07-23 01:18:29'),
(23, 30, 'CL2CR3', '2017-07-23 01:19:54', '2017-07-23 01:19:54');

-- --------------------------------------------------------

--
-- Structure de la table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fraisscolares`
--

CREATE TABLE `fraisscolares` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `montant` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `horaires`
--

CREATE TABLE `horaires` (
  `id` int(10) UNSIGNED NOT NULL,
  `cour_id` int(10) UNSIGNED NOT NULL,
  `debut` date NOT NULL,
  `fin` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_06_15_204632_create_cours_table', 1),
(9, '2017_06_25_180157_create_profiles_table', 1),
(10, '2017_06_26_202341_create_horaires_table', 1),
(11, '2017_06_26_202408_create_classes_table', 1),
(12, '2017_06_26_202510_create_bulletins_table', 1),
(13, '2017_06_26_202550_create_logs_table', 1),
(14, '2017_06_26_202607_create_comments_table', 1),
(15, '2017_06_26_202931_create_notifications_table', 1),
(16, '2017_06_26_203043_create_fraisscolaire_table', 1),
(17, '2017_06_26_203117_create_evaluations_table', 1),
(18, '2017_06_26_203156_create_notes_table', 1),
(19, '2017_06_26_204329_create_discussions_table', 1),
(20, '2017_07_13_191627_create_prof_infos_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cour_id` int(10) UNSIGNED NOT NULL,
  `u_from` int(10) UNSIGNED NOT NULL,
  `u_to` int(10) UNSIGNED NOT NULL,
  `bulletin_id` int(10) UNSIGNED NOT NULL,
  `note` int(11) NOT NULL,
  `discussion_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`id`, `nom`, `cour_id`, `u_from`, `u_to`, `bulletin_id`, `note`, `discussion_id`, `created_at`, `updated_at`) VALUES
(1, 'Intra', 1, 34, 8, 6, 78, 1, '2017-07-20 03:20:57', '2017-07-20 03:20:57'),
(2, 'Mini test', 1, 34, 8, 6, 85, 2, '2017-07-20 03:21:13', '2017-07-20 03:21:13'),
(3, 'Final', 1, 34, 8, 6, 65, 3, '2017-07-20 03:23:12', '2017-07-20 03:23:12'),
(4, 'Intra', 1, 34, 46, 2, 99, 4, '2017-07-20 03:23:41', '2017-07-20 03:23:41'),
(5, 'Mini test', 1, 34, 46, 2, 88, 5, '2017-07-20 03:24:33', '2017-07-20 03:24:33'),
(6, 'Final', 1, 34, 46, 2, 92, 6, '2017-07-20 03:24:53', '2017-07-20 03:24:53'),
(7, 'Intra', 1, 34, 19, 7, 79, 7, '2017-07-21 07:56:16', '2017-07-21 07:56:16'),
(8, 'Final', 1, 34, 19, 7, 99, 8, '2017-07-21 17:39:51', '2017-07-21 17:39:51'),
(9, 'Mini test', 1, 34, 19, 7, 98, 9, '2017-07-21 17:40:37', '2017-07-21 17:40:37'),
(10, 'Intra', 2, 32, 46, 3, 89, 10, '2017-07-21 21:37:34', '2017-07-21 21:37:34'),
(11, 'Intra', 1, 30, 45, 8, 85, 11, '2017-07-22 01:25:43', '2017-07-22 01:25:43'),
(12, 'Final', 1, 30, 45, 8, 98, 12, '2017-07-22 01:25:54', '2017-07-22 01:25:54'),
(14, 'Intra', 2, 34, 45, 4, 89, 14, '2017-07-23 00:24:21', '2017-07-23 00:24:21'),
(15, 'Quiz', 2, 34, 45, 4, 75, 15, '2017-07-23 00:24:37', '2017-07-23 00:24:37'),
(16, 'Final', 2, 34, 45, 4, 97, 16, '2017-07-23 00:24:49', '2017-07-23 00:24:49'),
(17, 'Intra', 1, 34, 45, 4, 89, 17, '2017-07-23 00:27:20', '2017-07-23 00:27:20'),
(18, 'Final', 1, 34, 45, 4, 79, 18, '2017-07-23 00:27:27', '2017-07-23 00:27:27'),
(19, 'Quiz 1', 3, 30, 21, 9, 77, 19, '2017-07-23 01:15:59', '2017-07-23 01:15:59'),
(20, 'Intra', 3, 30, 21, 9, 75, 20, '2017-07-23 01:16:58', '2017-07-23 01:16:58'),
(21, 'Quiz 2', 3, 30, 21, 9, 100, 21, '2017-07-23 01:17:13', '2017-07-23 01:17:13'),
(22, 'Final', 3, 30, 21, 9, 80, 22, '2017-07-23 01:18:29', '2017-07-23 01:18:29'),
(23, 'Quiz 1', 3, 30, 8, 6, 90, 23, '2017-07-23 01:19:54', '2017-07-23 01:19:54');

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `classe_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `matricule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sexe` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `infos` longtext COLLATE utf8_unicode_ci,
  `hasimg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `classe_id`, `parent_id`, `matricule`, `birthday`, `sexe`, `infos`, `hasimg`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 'ZAN2017071317', '2017-07-22', 'M', NULL, 'user-1.jpg', '2017-07-03 18:08:04', '2017-07-22 20:41:57'),
(2, 2, 0, 0, 'THE2017072032', '2017-07-22', NULL, NULL, 'user-2.jpg', '2017-07-03 18:10:55', '2017-07-22 18:02:53'),
(3, 3, 0, 0, 'CHA2017071918', '2017-07-19', NULL, NULL, NULL, '2017-07-03 18:11:30', '2017-07-03 18:11:30'),
(4, 4, 0, 0, 'GOU2017061215', '2017-07-21', 'F', NULL, 'user-4.jpg', '2017-07-03 18:12:44', '2017-07-22 00:59:08'),
(5, 5, 0, 0, 'CLO2017070213', '2017-07-02', NULL, NULL, NULL, '2017-07-03 18:13:36', '2017-07-03 18:13:36'),
(6, 6, 0, 0, 'RUE2017070334', '2017-07-03', NULL, NULL, NULL, '2017-07-03 18:14:14', '2017-07-03 18:14:14'),
(7, 7, 0, 0, 'CHA2017070586', '2017-07-21', 'F', NULL, 'user-7.jpg', '2017-07-03 18:14:46', '2017-07-22 00:43:25'),
(8, 8, 2, 37, 'HIC2017070461', '2017-07-21', 'F', NULL, 'user-8.jpg', '2017-07-03 18:17:53', '2017-07-22 01:20:17'),
(9, 9, 0, 0, 'ROU2014061075', '2017-07-21', NULL, NULL, 'user-9.jpg', '2017-07-03 18:18:43', '2017-07-22 00:53:11'),
(10, 10, 0, 0, 'MED2017072937', '2017-07-29', 'M', NULL, NULL, '2017-07-03 18:20:58', '2017-07-03 18:20:59'),
(11, 11, 0, 0, 'BOG2017070376', '2017-07-03', NULL, NULL, NULL, '2017-07-03 18:22:59', '2017-07-03 18:22:59'),
(12, 12, 0, 0, 'TRU2017070370', '2017-07-03', NULL, NULL, NULL, '2017-07-03 18:24:27', '2017-07-03 18:24:27'),
(13, 13, 1, 35, 'VLA2017060549', '2003-07-06', 'M', NULL, 'user-13.jpg', '2017-07-03 18:28:10', '2017-07-23 01:11:56'),
(14, 14, 0, 0, 'GAG2017070127', '2017-07-01', NULL, NULL, NULL, '2017-07-03 18:29:01', '2017-07-03 18:29:01'),
(15, 15, 0, 0, 'ZAP2013071037', '2017-07-21', 'F', NULL, 'user-15.jpg', '2017-07-03 18:29:48', '2017-07-22 00:41:46'),
(16, 16, 0, 0, 'ZOG2017070958', '2017-07-21', NULL, NULL, 'user-16.jpg', '2017-07-03 18:30:32', '2017-07-22 00:37:00'),
(17, 17, 0, 0, 'OUE2012070463', '2012-07-04', NULL, NULL, NULL, '2017-07-03 18:31:23', '2017-07-03 18:31:23'),
(18, 18, 0, 0, 'BEA2013071017', '2017-07-07', 'M', NULL, 'user-18.jpg', '2017-07-03 18:32:09', '2017-07-07 23:36:32'),
(19, 19, 1, 41, 'CLA201707043', '2017-07-06', 'F', NULL, 'user-19.jpg', '2017-07-03 18:33:28', '2017-07-07 00:29:15'),
(20, 20, 1, 39, 'HAM2017070567', '2017-07-21', 'M', NULL, 'user-20.jpg', '2017-07-03 18:42:25', '2017-07-22 00:34:46'),
(21, 21, 2, 48, 'MAD2017071450', '2017-07-22', 'M', NULL, 'user-21.jpg', '2017-07-03 18:43:37', '2017-07-23 01:22:32'),
(22, 22, 1, 48, 'BEA2012072687', '2000-04-02', 'M', NULL, 'user-22.jpg', '2017-07-03 18:44:25', '2017-07-23 01:36:42'),
(23, 23, 0, 0, 'HUG2017070914', '2017-07-09', NULL, NULL, NULL, '2017-07-03 18:58:39', '2017-07-03 18:58:39'),
(24, 24, 0, 0, 'ROT2017040426', '2017-04-04', NULL, NULL, NULL, '2017-07-03 18:59:18', '2017-07-03 18:59:18'),
(25, 25, 0, 0, 'HUM2017070694', '2017-07-22', 'F', NULL, 'user-25.jpg', '2017-07-03 19:01:55', '2017-07-23 01:09:11'),
(26, 26, 0, 0, 'GRO2017071427', '2017-07-21', NULL, NULL, 'user-26.jpg', '2017-07-03 19:05:05', '2017-07-22 01:18:10'),
(27, 27, 0, 0, 'MAL201707029', '2017-07-02', NULL, NULL, NULL, '2017-07-03 19:06:25', '2017-07-03 19:06:25'),
(28, 28, 0, 0, 'RAP2012071280', '2017-07-21', NULL, NULL, 'user-28.jpg', '2017-07-03 19:07:15', '2017-07-22 01:16:55'),
(29, 29, 0, 0, 'WHO2017071922', '2017-07-19', NULL, NULL, NULL, '2017-07-03 19:08:05', '2017-07-03 19:08:05'),
(30, 30, 0, 0, 'ZAN2017070263', '1991-08-18', 'F', NULL, NULL, '2017-07-03 19:08:49', '2017-07-23 01:06:09'),
(31, 31, 0, 0, 'GRO2017030175', '2017-03-01', NULL, NULL, NULL, '2017-07-03 19:10:33', '2017-07-03 19:10:33'),
(32, 32, 0, 0, 'DIO2014070327', '2017-07-21', 'M', NULL, 'user-32.jpg', '2017-07-03 19:11:14', '2017-07-21 21:40:27'),
(33, 33, 0, 0, 'Cé201207199', '2012-07-19', NULL, NULL, NULL, '2017-07-03 19:11:54', '2017-07-03 19:11:54'),
(34, 34, 0, 0, 'MEL201706144', '1990-04-23', 'F', NULL, 'user-34.jpg', '2017-07-03 19:13:29', '2017-07-23 01:01:22'),
(35, 35, 0, 0, 'BAR2011070696', '2017-07-21', NULL, NULL, 'user-35.jpg', '2017-07-03 19:15:36', '2017-07-22 01:14:01'),
(36, 36, 0, 0, 'MAR2012070397', '2012-07-03', NULL, NULL, NULL, '2017-07-03 19:16:08', '2017-07-03 19:16:08'),
(37, 37, 0, 0, 'HOU2011070689', '2011-07-06', NULL, NULL, NULL, '2017-07-03 19:16:35', '2017-07-03 19:16:35'),
(38, 38, 0, 0, 'SOB2011070765', '2017-07-21', 'F', NULL, 'user-38.jpg', '2017-07-03 19:17:08', '2017-07-21 20:37:42'),
(39, 39, 0, 0, 'TRU2017070268', '2017-07-16', 'F', NULL, NULL, '2017-07-03 19:17:36', '2017-07-16 23:41:47'),
(40, 40, 0, 0, 'MAR2017021627', '2017-02-16', NULL, NULL, NULL, '2017-07-03 19:18:19', '2017-07-03 19:18:19'),
(41, 41, 0, 0, 'CUA2017050344', '2017-07-22', 'M', NULL, 'user-41.jpg', '2017-07-03 19:18:51', '2017-07-23 00:30:16'),
(42, 42, 0, 39, 'BUC2017072682', '2017-07-21', 'M', NULL, 'user-42.jpg', '2017-07-03 19:21:43', '2017-07-22 00:30:50'),
(43, 43, 0, 0, 'COU2010070679', '2017-07-21', 'F', NULL, 'user-43.jpg', '2017-07-03 19:31:30', '2017-07-22 00:24:39'),
(44, 44, 0, 39, 'RIO2017070894', '1999-10-29', 'M', NULL, 'user-44.jpg', '2017-07-03 20:01:00', '2017-07-23 00:50:40'),
(45, 45, 1, 41, 'TRU201007215', '2017-07-22', 'M', NULL, 'user-45.jpg', '2017-07-03 20:03:06', '2017-07-23 00:46:55'),
(46, 46, 3, 38, 'BLA2010070158', '2000-02-04', 'F', NULL, 'user-46.jpg', '2017-07-03 23:54:31', '2017-07-23 00:58:35'),
(47, 48, 0, 0, 'BOO2017071258', '2017-07-22', 'M', NULL, 'user-48.jpg', '2017-07-13 00:53:14', '2017-07-23 00:49:16');

-- --------------------------------------------------------

--
-- Structure de la table `prof_infos`
--

CREATE TABLE `prof_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `classe_id` int(10) UNSIGNED NOT NULL,
  `cour_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `prof_infos`
--

INSERT INTO `prof_infos` (`id`, `user_id`, `classe_id`, `cour_id`, `created_at`, `updated_at`) VALUES
(1, 34, 1, 1, '2017-07-15 15:37:21', '2017-07-15 15:37:21'),
(2, 34, 1, 2, '2017-07-15 15:37:21', '2017-07-15 15:37:21'),
(9, 34, 2, 6, '2017-07-23 01:00:25', '2017-07-23 01:00:25'),
(4, 32, 3, 1, '2017-07-21 21:35:46', '2017-07-21 21:35:46'),
(5, 32, 3, 2, '2017-07-21 21:35:46', '2017-07-21 21:35:46'),
(10, 25, 3, 5, '2017-07-23 01:08:55', '2017-07-23 01:08:55'),
(7, 30, 2, 3, '2017-07-22 01:23:25', '2017-07-22 01:23:25'),
(8, 30, 2, 4, '2017-07-22 01:23:25', '2017-07-22 01:23:25'),
(11, 25, 2, 13, '2017-07-23 01:09:11', '2017-07-23 01:09:11');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'eleve',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `lname`, `email`, `password`, `role`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Zannou', 'Aurele', 'admin@diginote.com', '$2y$10$FTfprqkE1kEfM7ICyFPxx.4cgjI5JqaT4z71uY72Y.DzcrQoL0vny', 'admin', 1, 'MgfRWivaNTPJLecqJMhSVtXR0lQkCcIdqngHJRNwOpI6l338CRVkZx9DmreR', '2017-07-03 18:08:04', '2017-07-16 23:02:16'),
(2, 'Theriault', 'Alain', 'alain@diginote.com', '$2y$10$/94UpEIYO3LVQPHshscmZOKqKNKAqeIOsl1oamC4df8Zq5ycQC0Sm', 'eleve', 0, 'QvsvMY3IjaMlyriHQqZxnRKGYnybMC8fNEmQ4QxUb5QiuhMfRMf7AcVw0MqL', '2017-07-03 18:10:55', '2017-07-03 18:10:55'),
(3, 'Charron', 'David', 'david@diginote.com', '$2y$10$fJAQ6yRyVSFa7j5MoDBV9uRcgzRI2PaA4sa2BSFgSgnyd4mSR16P.', 'eleve', 0, 'q5gDIhIRYppRbvYUBxRlV523Jb28rtU8OZJEodYziorsByu3oC5mXfzNiyhJ', '2017-07-03 18:11:30', '2017-07-03 18:11:30'),
(4, 'Gougeon', 'Jean', 'jean@diginote.com', '$2y$10$mMNy3wdVlKRv5ekdvX81beHWFzflrpRcRIgpLBPMHgzF9TSAAHGhu', 'eleve', 0, 'rAU8M9JC795vaMOSXbBLlk5OGezBo8QelMm7QrceSvirdHGnH4OHLfaLAiuJ', '2017-07-03 18:12:44', '2017-07-03 18:12:44'),
(5, 'Clothilde', 'Eito', 'eito@diginote.com', '$2y$10$5Nt4p6uFd1WTQmi.XUAax.8AJhZnwXSEz9IN8/pIRYu33HMXlt4ae', 'eleve', 0, 'K7sk1J9jb2NP8Zvctyro7xSLDkmxNSFLnMhFZLXJ6EzBAHdmjGKTIAbWLGJ0', '2017-07-03 18:13:36', '2017-07-03 18:13:36'),
(6, 'Ruel', 'Marc', 'marc@diginote.com', '$2y$10$cqfM/bQ1.sSo9OcigFzxCuIy7OFKs0j0gEU6z0ZCifIQVMeVTQ1qO', 'eleve', 0, 'sFDERf9b28SCzlU5Lj4TdHUnSwYQ7MpIfJ5R9ryd6xAiMuWWeraNc2ioYAUi', '2017-07-03 18:14:14', '2017-07-03 18:14:14'),
(7, 'Charton', 'Anne', 'anne@diginote.com', '$2y$10$Zj3aBCdGFHsPxbkpKi9TaeLCbw2JVJb1zbN/Tmf7O7pAlMDPm38u2', 'eleve', 0, 'UqFEcLVecJjEL1U7kvdKy0pXePGx6dcDkLiwGtdLTZMc4WTPCiiCef4YxZh9', '2017-07-03 18:14:46', '2017-07-03 18:14:46'),
(8, 'Hickram', 'Oualid', 'oualid@diginote.com', '$2y$10$dg6rrZ8g4FlIpXOpYbXq7eZZQYS/QwzUJWhLNXJ/pleJKMs31OA/y', 'eleve', 1, '5D3mK0NJNnVdDZFBSgGByxEWTI133i2XAMy65DMFKj2DUh0BexOYbe5kgZiV', '2017-07-03 18:17:53', '2017-07-07 01:42:49'),
(9, 'Rouleau', 'Daniel', 'daniel@diginote.com', '$2y$10$iryypopPGuH8KGR4ujDzh.hLOpGa1ABiPZDH23JwM6c5/p8aoUTH2', 'eleve', 0, '9myKNhSCHNaFy9aqxxFcjfwM1HsA59pxWlR74UPqQUXRahaSaYMRj48z5Gx0', '2017-07-03 18:18:43', '2017-07-03 18:18:43'),
(10, 'Medars', 'Paul', 'paul@diginote.com', '$2y$10$kJ8GeBwyv5XgwXxtSgmh3.VRFp3QgCKuBmUdfQAghWYx7BD4rRVs6', 'eleve', 1, NULL, '2017-07-03 18:20:58', '2017-07-03 18:20:59'),
(11, 'Bogne', 'Yannick', 'yannick@diginote.com', '$2y$10$4jCm7L9znAowh2iqCm84ru5vEOkMQGC/.K4i2a2EaVYEGW1Sux.Aq', 'eleve', 0, 'wOWIzz8Rf4T6BclVRe06Pz2F2PSTXyqo5l3XjW7m4XqNWLoPSj6ILmg85BTN', '2017-07-03 18:22:59', '2017-07-03 18:22:59'),
(12, 'Trumpirus', 'Donalus', 'donald@diginote.com', '$2y$10$CmJ.VIRIK52zQHpPk5rxuuUsVlzT5.Bn.D6D18OTjS/o/WFFVUX9m', 'eleve', 0, 'DHQezKY7QZLcFmdq3cYFIKmafi5ePOlG2xbNrmUEXWpx5VKVKhSUVdDWI66r', '2017-07-03 18:24:27', '2017-07-03 18:24:27'),
(13, 'Vladimus', 'Poutinus', 'poutinus@diginote.com', '$2y$10$s8A1u1BAcg3gX2fvZkHKpuiKGe06/i1OA8EuD/X9ErE9XC2m1TABW', 'eleve', 1, 'AAPQ7ymqetqJiiZMWWj3xU1Z0RR6OVpkEZBQCIqgGQ5s5iw663JnO1ykpT7A', '2017-07-03 18:28:10', '2017-07-22 00:42:11'),
(14, 'Gagnon', 'Darius', 'darius@diginote.com', '$2y$10$VBhd7CjYoLlVhvaZb79kouBOFIzN/cjQ.g.XvauS7z9O5.fLE5mHy', 'eleve', 0, 'wfWxAP2uMbbobJD5TCCdSIpVRdand81B7Nh0bBLtUvPCn07rHp8EwwSW7heI', '2017-07-03 18:29:01', '2017-07-03 18:29:01'),
(15, 'Zappa', 'Francine', 'francine@diginote.com', '$2y$10$6o9WhNWR4unkqk93RIC5fObqNRGClLAYkf8D1.IyTXaaQjimVgvga', 'eleve', 0, 'fameqTK5Q1Kz22bXGmukngQX6tFT7ElySqncqijRbpFXyR5qFZncqmyr8GRC', '2017-07-03 18:29:48', '2017-07-22 00:41:46'),
(16, 'Zogang', 'Francis', 'francis@diginote.com', '$2y$10$DtA89T8XBZSGV1sr3TheNON/8CfT7eSA9K./ZLj./.yC/oh1RH.re', 'eleve', 0, 'dkc2IBh8m4NGiaC4kCUf0XOUKPJ5NrxvOv7ONzZrBeOLsgKltPB0L4TKtCtF', '2017-07-03 18:30:32', '2017-07-03 18:30:32'),
(17, 'Ouelette', 'Francky', 'francky@diginote.com', '$2y$10$6MrbPxgLDwlbPc4IRNnZz.Y841x2vbvpaJlekL1Q9azPdtYg9hT/G', 'eleve', 0, 'gxsNI1r3kXR08u1B9tzYd5sOR5lFxE8dB9TvkrwnZDH5PpLwSdyauQxQveMs', '2017-07-03 18:31:23', '2017-07-03 18:31:23'),
(18, 'Beaudry', 'Marcus', 'marcus@diginote.com', '$2y$10$hth25MrzKuj9J/Armrk5E.PqLLeUXkpPdVdveE5UA/jQV1k91OT6u', 'eleve', 1, 'lg1ygnCh27jP86FXLtfaYsuj4p5RJ5n7zBisOEbWUd8hwGevNP2G0Q889ncO', '2017-07-03 18:32:09', '2017-07-07 23:36:32'),
(19, 'Clark', 'Adam', 'adam@diginote.com', '$2y$10$FGop7O4peVN9xVmNyCoB/eZ5Nay.Gz6GkCl4eS93xuUPGZtwkC9rS', 'eleve', 1, '63wizLRSfWGINBwLQDPUwxVSZjl6TK7e3PN5q6AtPUAxnNeHIicJAVzKTXtD', '2017-07-03 18:33:28', '2017-07-07 00:29:15'),
(20, 'Hammad', 'Amine', 'amine@diginote.com', '$2y$10$xJr1Xa5RktohJqjL2fz9Oe3edDP.khzXQZkb4EAycoH1MgyPwMqWi', 'eleve', 1, NULL, '2017-07-03 18:42:25', '2017-07-03 18:42:25'),
(21, 'Madode', 'Aubry', 'axelmadode@gmail.com', '$2y$10$Qt.KhvUcH62a1lKb2Y0kN.dgtiyHE8weDpp8OpocD6svx3GC9fVGG', 'eleve', 1, 'NYI8BkHxjUZLpu1Hs76Q7suBQXLrOybyBuukwKvJtr9NBYdpbwxCOyRXlBD7', '2017-07-03 18:43:37', '2017-07-23 01:22:32'),
(22, 'Beaudry', 'Douglass', 'douglass@diginote.com', '$2y$10$GDOFumjCrfsviLadGtsZ2.Fwl1Hs60H9YEzgLoPvi..aihBG8YAay', 'eleve', 1, NULL, '2017-07-03 18:44:25', '2017-07-03 18:44:25'),
(23, 'Hugh', 'Jass', 'jass@diginote.com', '$2y$10$lg7lc1PPqQ3ZLBQ5GuRXCOpiMk5iIdGcTvjz.akpIuRMlp26kET8C', 'professor', 0, 'fnuxM0sFTIIz6AoJVSZIWZStGsYXKS2F3M6xMutSK4I3bu5QlOsrRQo2iC9V', '2017-07-03 18:58:39', '2017-07-03 18:58:39'),
(24, 'Rotch', 'Mike', 'mike@diginote.com', '$2y$10$RUB1Upk.8PV05ZWHp6TeputeA.Qwo1A6R79ueA1UYBm.f9D1PyMjS', 'professor', 0, 'SGrFTkak86nfpwzavqs22Jn7VhCkWnCOn1LlOO0Z8FxcP40MoPuF9gFAOGl5', '2017-07-03 18:59:18', '2017-07-03 18:59:18'),
(25, 'Humpalot', 'Ivana', 'ivana@diginote.com', '$2y$10$zmh.od3CkSc4Eb8v2f6QGuqJGrTSb0681vEzFC2ucGIG7.KSjRSzq', 'professor', 1, 'iEUDJrhburEIrGVLIU9oH1z4M6a1Miria2r3cN6uFvkojf8E4Gr4ymDFWdg1', '2017-07-03 19:01:55', '2017-07-22 01:18:43'),
(26, 'Grognon', 'Judith', 'judith@diginote.com', '$2y$10$sVF/.t2BM7t0io4.ChRR5eRXF4vJp8HtutXZ8kBXgZ1dCYzJ552XO', 'professor', 0, 'Ff4k6r6fqCCgOGWp5RmF5wFNdbxlGf3z4m4ajbHO0LqvthvR7YwW6Zc9mYAL', '2017-07-03 19:05:05', '2017-07-03 19:05:05'),
(27, 'Malou', 'Rodrigue', 'malou@diginote.com', '$2y$10$sWp3fzLtszdDSo9Cx1yJ/.gxk0ABilRZBY60kJD2XpS5chXkhZQn2', 'professor', 0, 'B2LwiRlfV4D1v4VjDziuuzFlASk1yL3cuM2BKbHzEMyVOGObEszSi8oLdM2T', '2017-07-03 19:06:25', '2017-07-03 19:06:25'),
(28, 'Rappard', 'Rosette', 'rosette@diginote.com', '$2y$10$3rjIJHaWL6n3zcugNnqXTOrH/N8ZvH90F9K/aYU3xlFxtBCyfezlK', 'professor', 0, 'PdtTgPu4kXG9FLHKoveceyBB6HVpROy6oi3JlSYNYawLHAreQVVfBuOfzBsW', '2017-07-03 19:07:15', '2017-07-03 19:07:15'),
(29, 'Whoo', 'Anita', 'anita@diginote.com', '$2y$10$gretzkkjuUcX83ds9dut8ObgmWoKmfObEoJvu3maS/AujAYjEOMta', 'professor', 0, 'lQuiWkYo0x8RAB1K2shA0WQqnJ2QqpRQQUcHtbm05lTMizB2SvlxHUMk8M89', '2017-07-03 19:08:05', '2017-07-03 19:08:05'),
(30, 'Zanage', 'Ariel', 'ariel@diginote.com', '$2y$10$OWUto9otgSpN4MGP2aSQZevQSQAkC6NMC7bP42mBJvheK6ADSKw4S', 'professor', 1, 'yxtksxMjCawpZbDGCJfBhZzP1XsUfmTAB6ort9jNvihFW4QIhEyz3lMOWlw4', '2017-07-03 19:08:49', '2017-07-15 15:37:02'),
(31, 'Grognon', 'Raul', 'raul@diginote.com', '$2y$10$SWwbBW9LW0nFf33/xgSUJujBDC35K/mxXo93VOhOzvmEr.QllPHfi', 'professor', 0, 'empT5uB8DDXdzTZu7ywtq5Ga7AdRZ78clckEnd5qZcVewcxFE1Z0jBjurrqG', '2017-07-03 19:10:33', '2017-07-03 19:10:33'),
(32, 'Diop', 'Booba', 'diop@diginote.com', '$2y$10$ivq0caQvWASzxyfOlkU41eYOT5ABjP5ekokqGbB3aUuMgjxedljqC', 'professor', 1, '8Hu5PleRQdMikKPI15QP2QOhImQHktmXtdDSHhCaoNqeMSPFidp1bBjnnCWQ', '2017-07-03 19:11:14', '2017-07-21 21:35:47'),
(33, 'Césair', 'Aimé', 'aime@diginote.com', '$2y$10$cbBIJ9SjimYtAUV7zwn.keKlG6dFURarACJkJPkG2QYzZkFQizxzy', 'professor', 0, 'I1KLPMpfE2qTdkE0ZsQdNTt9XNvs7Mtf9FV01RYzRr8I82wAVjrbxCTcHm10', '2017-07-03 19:11:54', '2017-07-03 19:11:54'),
(34, 'Melanc', 'Miranda', 'miranda@diginote.com', '$2y$10$.D9bg.z8kFaKpyxvAFsrbuE2fWcJbgrFrvWdnVmlBAbYHgdKX0Zs2', 'professor', 1, 'byCzQDkcIgDWePbyQwaODeaQicJalnMK7FDe66Lk4o40q9GLQB8DrXjVc46r', '2017-07-03 19:13:29', '2017-07-23 00:22:09'),
(35, 'Barrete', 'Nadia', 'nadia@diginote.com', '$2y$10$Ne0aKZUa9ptZmO7YWIqF3eSxL.vHtfjRsAW4ZaE7aKPiICuL3xBQ2', 'parent', 0, 'pQJZwg1QSlMe5osl3zifiDe7y8KdnbV5MufMziW4QSzNOwTGW5LOzFVQqsvo', '2017-07-03 19:15:36', '2017-07-03 19:15:36'),
(36, 'Marchal', 'Cyano', 'cyano@diginote.com', '$2y$10$ljBBmjnSgU5Z6MUY8zA/8uRHzCCyNfkUeIWKIz49S0gCTCcipVYma', 'parent', 0, 'l7fBafCyxkysTQbBBqoAqupcNj0Sr4YOHRx0oOM12s9Fsi3khRwpuoNtma7E', '2017-07-03 19:16:08', '2017-07-03 19:16:08'),
(37, 'Housso', 'Junior', 'junior@diginote.com', '$2y$10$CRHQuLuq5bGxxKtbiBLw..ycCzVojY.2Q5fNklienaT8QcxC4jPom', 'parent', 0, 'K8oJHtqFB2Ex60m4IGih43xM4tDOMLjthv7DKIr6KsxtwqEjuxdw84CcQ6DE', '2017-07-03 19:16:35', '2017-07-03 19:16:35'),
(38, 'Sobre', 'Mélodie', 'mel@diginote.com', '$2y$10$6fmbsrDGOHMtG8ix6KmvLO5R5sncKevMDZ4/09IWa75Nzw25ThsLW', 'parent', 1, 'T8dmcRmvIwL8VFyT0n4CuN9ZLK7T4OdZfg1crmte4whrcRNHXdVpRrSI79bL', '2017-07-03 19:17:08', '2017-07-21 20:37:42'),
(39, 'Trudeau', 'Gracia', 'gracia@diginote.com', '$2y$10$rwhFBS9IaQ1UQt.c2b0jqOaMQnoRUsjMUP21IFzd95fHfTIXmMVxO', 'parent', 1, 'gwQEViMF9jEdeWbeoBhfOFzLrCmURhADX261r263Trtc6pbLamBh8FjzPhOR', '2017-07-03 19:17:35', '2017-07-03 19:17:35'),
(40, 'Maroiluss', 'Pauline', 'pauline@diginote.com', '$2y$10$5uaGfuphg62TepcqPvQinOtpME7uOa2WCT7aXl2Gt29TEH5lyu3RS', 'parent', 0, 'cIcIIrLbGp2UWXHijLmT6QuGtmirhxokrc5hAxQhOQzBl9ysHhl3iH2qKXju', '2017-07-03 19:18:19', '2017-07-03 19:18:19'),
(41, 'Cuatro', 'Justin', 'justin@diginote.com', '$2y$10$lrbkj.FQaqxtchyuqpJTluN67Gvbvax.tMvziL46SoVTstFrKuc5K', 'parent', 1, 'w0QZ9sSpEcphYlPgAwZWVnzRnyLmoelEMES1dEMVUkuDZvvYAMDIjELHUSxv', '2017-07-03 19:18:51', '2017-07-23 00:30:16'),
(42, 'Buchette', 'Martin', 'martin@diginote.com', '$2y$10$cL4D31O2mLdaWwafR56v9OV6OAmXkVkJFKPKMMx.A6nDCzkFl6ge.', 'eleve', 0, NULL, '2017-07-03 19:21:43', '2017-07-06 00:18:50'),
(43, 'Couillard', 'Patricia', 'patricia@diginote.com', '$2y$10$cfHF508sWCyZR2g8rx9mlO7Qci7.bc4PRIJSLDdYuvRuIJFkyPv8m', 'eleve', 0, NULL, '2017-07-03 19:31:30', '2017-07-03 19:31:30'),
(44, 'Riot', 'Boris', 'boris@diginote.com', '$2y$10$xEHOkJdXkaaSb.qNEPnBGeEipOqaErekEYmTl2EzI9PYGpBJYaXiS', 'eleve', 1, 'ZilTSYBFXjggh8w2jXIyNrJ0WSKhPYMJKY8zcokcXtWogoRO8pQQ1Z143cIP', '2017-07-03 20:01:00', '2017-07-16 23:45:41'),
(45, 'Trudeau', 'Simon', 'simon@diginote.com', '$2y$10$B5M0ZkY3yOXuD9qdyBdXZucSLRdkSTF6ES2DMWCT0RKb4syEg.bmC', 'eleve', 1, 'G1glfYtai9fqZSQbFUo4xYh4gQ9O41Au1md4XFT0xDccgZSGmdSXWMEbjITm', '2017-07-03 20:03:06', '2017-07-22 01:24:40'),
(46, 'Black', 'Meduse', 'black@diginote.com', '$2y$10$fJjZvSF7scwIKydfLcM4juoK4k55ts9DdzqhFGx6MurvHUMlfZme2', 'eleve', 1, 'iaolJYunoXpAnXOGiEkTALH3k59OB4stptw2y1USCdYLRGfNMequs0Go1fTn', '2017-07-03 23:54:31', '2017-07-21 22:52:17'),
(48, 'Boogie', 'Joe', 'joeb@diginote.com', '$2y$10$X0Oyov9Jo8F1PfpfSWMCCeeBMII8HotPIHRVjvnJV2tmqufYL9xS.', 'parent', 1, 'oxow5HE5eXUQbX9O3D0da0Gg138x9ZAtDsTqNnoSb3DXywI979R8L60ZKCLw', '2017-07-13 00:53:14', '2017-07-13 00:53:14');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bulletins`
--
ALTER TABLE `bulletins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bulletins_user_id_index` (`user_id`),
  ADD KEY `classe_id` (`classe_id`);

--
-- Index pour la table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_index` (`user_id`),
  ADD KEY `comments_discussion_id_index` (`discussion_id`);

--
-- Index pour la table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cours_code_unique` (`code`),
  ADD KEY `cours_classe_id_index` (`classe_id`);

--
-- Index pour la table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discussions_note_id_index` (`user_id`);

--
-- Index pour la table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fraisscolares`
--
ALTER TABLE `fraisscolares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fraisscolares_user_id_index` (`user_id`);

--
-- Index pour la table `horaires`
--
ALTER TABLE `horaires`
  ADD PRIMARY KEY (`id`),
  ADD KEY `horaires_cour_id_index` (`cour_id`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notes_cour_id_index` (`cour_id`),
  ADD KEY `notes_bulletin_id_index` (`bulletin_id`),
  ADD KEY `u_from` (`u_from`),
  ADD KEY `u_to` (`u_to`),
  ADD KEY `discussion_id` (`discussion_id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Index pour la table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_index` (`user_id`),
  ADD KEY `profiles_classe_id_index` (`classe_id`),
  ADD KEY `profiles_parent_id_index` (`parent_id`);

--
-- Index pour la table `prof_infos`
--
ALTER TABLE `prof_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prof_infos_user_id_index` (`user_id`),
  ADD KEY `prof_infos_classe_id_index` (`classe_id`),
  ADD KEY `prof_infos_cour_id_index` (`cour_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bulletins`
--
ALTER TABLE `bulletins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `cours`
--
ALTER TABLE `cours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `fraisscolares`
--
ALTER TABLE `fraisscolares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `horaires`
--
ALTER TABLE `horaires`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT pour la table `prof_infos`
--
ALTER TABLE `prof_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
