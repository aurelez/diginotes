<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->integer('cour_id')->unsigned()->index();
            $table->integer('u_from')->unsigned()->index();
            $table->integer('u_to')->unsigned()->index();
            $table->integer('bulletin_id')->unsigned()->index();
            $table->integer('note');
            $table->integer('discussion_id')->default(0)->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
