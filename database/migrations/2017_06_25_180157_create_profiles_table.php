<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('classe_id')->default(0)->unsigned()->index();
            $table->integer('parent_id')->default(0)->unsigned()->index();
            $table->string('matricule')->nullable();
            $table->date('birthday')->nullable();
            $table->char('sexe', 1)->nullable();
            $table->longText('infos')->nullable();
            $table->string('hasimg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
