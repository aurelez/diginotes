/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

var digiapp = new Vue({
	el: '#digiapp',
	data: {
		s_url : window.s_url,

		elevesLisit: [],
		eleveSearch: '',

		coursList: (typeof(cours_fromBk) != "undefined") ? cours_fromBk : [], //this var comes directly from backend via this plug https://github.com/laracasts/PHP-Vars-To-Js-Transformer
		coursSearch: '',

		classesList: (typeof(classes_fromBk) != "undefined") ? classes_fromBk : [],
		classesSearch: '',

		parentsList: (typeof(parents_fromBk) != "undefined") ? parents_fromBk : [],
		parentsSearch: '',

		profsList: (typeof(profs_fromBk) != "undefined") ? profs_fromBk : [],
		profsSearch: '',
		eleveNotes: [],

		elemtormv: {
			id: '',
			name: ''
		},
		showbtndelelem: false,

		bullSearch: {
			cour: '',
			from: '',
			to: '',
			toname: ''
		},

		showSessMsg: true,
		showEleveNotesBox: false,
		showAddNoteBox: false,

		discussion_id: '',
		comments: [],
		showWrapBox: false,

		showSpinner: false,
	},

	// call when initialize
	created: function(){
		var elem = $('#viewWrap')

		//--
		if(elem.data('loadhelper') == 'eleves') this.fetchEleves()

		//--
		var discid_ = elem.data('targdiscid')
		if(typeof(discid_) != "undefined" && discid_ != ''){
			console.log(elem.data('targdiscid'), this.showWrapBox)
			this.discussion_id = elem.data('targdiscid')
            this.showWrapBox = true;
			this.getComments()
		}
	},

	computed: {
		computedCoursList: function(){
			var vm = this
			return this.coursList.filter(function (item) {
				return item.title.toLowerCase().indexOf(vm.coursSearch.toLowerCase()) !== -1
			})
		},

		computedClassesList: function(){
			var vm = this
			return this.classesList.filter(function (item) {
				return item.nom.toLowerCase().indexOf(vm.classesSearch.toLowerCase()) !== -1
			})
		},

		computedParentsList: function(){
			var vm = this
			return this.parentsList.filter(function (item) {
				return item.name.toLowerCase().indexOf(vm.parentsSearch.toLowerCase()) !== -1 || item.lname.toLowerCase().indexOf(vm.parentsSearch.toLowerCase()) !== -1
			})
		},

		computedProfsList: function(){
			var vm = this
			return this.profsList.filter(function (item) {
				return item.name.toLowerCase().indexOf(vm.profsSearch.toLowerCase()) !== -1 || item.lname.toLowerCase().indexOf(vm.profsSearch.toLowerCase()) !== -1
			})
		}
	},

	methods:{
		//--fetch eleve from backend
		fetchEleves: function(){
			var vm = this
			axios.get(this.s_url+'/api/getEleves?search='+this.eleveSearch)
				.then(function(response){
					//console.log(response.data.users.data)
					Vue.set(vm.$data, 'elevesLisit', response.data.users.data)
				})
				.catch(error => {
					console.log(error)
					alert("Axios request error!")
				});
		},

		//--get notes from backend
		getBulletin: function(){
			var vm = this
			axios.get(this.s_url+'/api/getNotes?cour='+this.bullSearch.cour+'&from='+this.bullSearch.from+'&to='+this.bullSearch.to)
				.then(function(response){
					//console.log(response.data.notes)
					Vue.set(vm.$data, 'eleveNotes', response.data.notes)
				})
				.catch(error => {
					alert("Axios request error!")
				});
		},

		//--getNotesMoy
		getNotesMoy: function(eleveNotes){
			let tot = 0
			for (let eleveNote of eleveNotes)
				tot += eleveNote.note
			return Math.round( (tot/eleveNotes.length) * 100 ) / 100
		},

		//--delNote
		delNote: function(){
			var vm = this
			axios.delete(this.s_url+'/api/delNote', {params: {id: this.elemtormv.id}})
				.then(function(response){
					console.log(response.data.ret)
					vm.getBulletin()
				})
				.catch(function (error) {
					alert(error);
				});
			this.showbtndelelem = false;
		},

		//--get comments from backend
		getComments: function(){
			var vm = this
			axios.get(this.s_url+'/api/getComments?discid='+this.discussion_id)
				.then(function(response){
					console.log(response.data.comments)
					Vue.set(vm.$data, 'comments', response.data.comments)
					Vue.set(vm.$data, 'showSpinner', false)
				})
				.catch(error => {
					alert("Axios request error!")
				});
		},

		//--anim cascade
		beforeEnter: function (el) {
			el.style.opacity = 0
			el.style.height = 0
		},
		enter: function (el, done) {
			var delay = el.dataset.index * 80
			setTimeout(function () {
				Velocity(
					el,
					{ opacity: 1, height: '42px' },
					{ complete: done }
				)
			}, delay)
		},
		leave: function (el, done) {
			var delay = el.dataset.index * 80
			setTimeout(function () {
				Velocity(
					el,
					{ opacity: 0, height: 0 },
					{ complete: done }
				)
			}, delay)
		}
	}
});


//----jq
$(document).ready(function() {
	//--
	$('select').material_select();
	$('input').attr('autocomplete', 'off');
	//$('.tooltipped').tooltip({delay: 50});

	//----
	$('.datepicker').pickadate({ //https://www.theinvestmentassociation.org/assets/components/ima_circularwizard/js/pickadate/docs.htm
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 28, // Creates a dropdown of 15 years to control year
		max: new Date(2018,1,1),
		
		formatSubmit: 'yyyy-mm-dd',
  		hiddenName: true,

		today: 'Today',
		clear: 'Clear',
		close: 'OK',
	});

	//---select firs elem of profcour notes
	$('.prof_cour .wrapenfents ul.enfants a:first-child').click();

	//---elevenotesubmit
	//-----------------------------------------------------------------
	var alreadysubmit=false;
	$("#addnoteform").on("submit", function(e){
		e.preventDefault();

		if(alreadysubmit) return;
		alreadysubmit = true;

		var $this = $(this);
		output = $this.find("#axios_output");
		//console.log($this.serialize())

		axios.post(window.s_url+'/api/postNote', $this.serialize())
			.then(function (response) {
				output.html(response.data.ret);
				$this.find('#nom, #note').val('');
				alreadysubmit = false;
				setTimeout(function(){
					output.empty();
				}, 15000);

				//notify vuejs
				digiapp.getBulletin();
			})
			.catch(function (error) {
				alert(error);
			});
	});

	//----toggle active class on discussion
	$('.digi_discussions .wrapenfents .enfant').click(function(){
		$('.digi_discussions .wrapenfents .enfant').removeClass('active');
		$(this).addClass('active');
		$('.digi_discussions .comments .dtitle').html($(this).html());
	});

	//----
	var elem = $('#viewWrap')
	var discid_ = elem.data('targdiscid')
	if(typeof(discid_) != "undefined" && discid_ != '')
		$('.disc_'+elem.data('targdiscid')).click()

	//---
	$('textarea#content').focus();
	$('textarea#content').trigger('autoresize');


	//--submitComment
	//-----------------------------------------------------------------
	$("#addcommentform").on("submit", function(e){
		e.preventDefault();

		if(alreadysubmit) return;
		alreadysubmit = true;

		var $this = $(this);
		output = $this.find("#axios_output");

		digiapp.showSpinner = true;

		axios.post(window.s_url+'/api/postComment', $this.serialize())
			.then(function (response) {
				alreadysubmit = false;
				$this.find('textarea').val('');
				//notify vuejs
				digiapp.getComments();
			})
			.catch(function (error) {
				alert(error);
			});
	});
});