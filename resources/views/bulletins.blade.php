@extends('layouts.beforesingle')

@section('beforesingle')

<?php 
	$user = $data['user'];
	$profile = $user->profile;
	$bulletins = $user->bulletin;
 ?>
<div id="viewWrap" class="viewWrap admin_eleve mainpad" data-loadhelper="">
	<div class="focused full_w">
		<h1><i class="fa fa-folder-open" aria-hidden="true"></i> Bulletins <span>{{ $user->name.' '.$user->lname }}</span></h1>
		<div class="additional full_w" style="margin-left:0;">
            <ul class="collapsible" data-collapsible="accordion">
                <li class="u_bulletins">
                    <div class="collapsible-header active"><b>Historique des Bulletins</b></div>
                    <div class="collapsible-body">
                    	@if(count($bulletins) > 0)
						<div class="card-tabs">
							<ul class="tabs tabs-fixed-width">
		                        @foreach($bulletins as $bulletin)
		                        	<li class="tab"><a href="#a{{$bulletin->annee}}">{{$bulletin->annee}}</a></li>
		                        @endforeach
							</ul>
						</div>
						<div class="card-content lighten-4">
	                        @foreach($bulletins as $bulletin)
								<div id="a{{$bulletin->annee}}">
									<div class="top">
										<h2><i aria-hidden="true" class="fa fa-user"></i> {{$user->name.' '.$user->lname}}</h2>
										<p><b>Année scolaire:</b> {{$bulletin->annee}}</p>
										<p><b>Classe:</b> {{$bulletin->classe->code.' '.$bulletin->classe->nom}}</p>
									</div>
									<table class="bordered highlight responsive-table notes">
										<thead>
											<tr>
											<th>Cours</th>
											<th>Éval.</th>
											<th>Note</th>
											</tr>
										</thead>
										<tbody>
											@foreach($bulletin->note()->orderBy('cour_id')->get() as $note)
											<tr>
												<td>{!!getCoursName($note->cour_id)!!}</td>
												<td>{{$note->nom}}</td>
												<td>{{$note->note}}/100</td>
											</tr>
											@endforeach
											<tr class="moy">
												<td></td>
												<td><b>Moyenne:</b></td>
												<td><b>{{getMoyenne($bulletin->note()->get())}}</b>/100</td>
											</tr>
										</tbody>
									</table>
								</div>
	                        @endforeach
						</div>
						@else <p style="padding:0 10px;">Aucun bulletin disponible</p> @endif
                    </div>
                </li>
            </ul>
		</div>
	</div>
	<!--  -->
	<div class="clear"></div>
</div>

@endsection