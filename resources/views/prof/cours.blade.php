@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_prof admin_classe prof_cours mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Cours </b></h1>
        <div class="row">
            <div class="s12 full_w profcours">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active">
                            <i aria-hidden="true" class="fa fa-folder-open "></i><b>Cours enseignés </b>({{ count($data['pcours']) }})
                        </div>
                        <div class="collapsible-body">
                            <ul>
                            @foreach($data['pcours'] as $cour)
                                <li><a href="{{ route('profCour', [$cour->classe_id, $cour->cour_id]) }}"><i class="fa fa-folder-open" aria-hidden="true"></i> <b>{{$cour->courcode}}</b> ({{$cour->classecode.'-'.$cour->classename}})</a></li>
                            @endforeach
                            </ul>
                            @if (count($data['pcours']) < 1) <p style="margin:0;">Aucun cours encore attribué!</p> @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<!--  -->
	<div class="clear"></div>
</div>

@endsection