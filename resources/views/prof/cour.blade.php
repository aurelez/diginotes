@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_classe prof_cour mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Cours: <b>{{ $data['cour']->code }}</b> {{ $data['cour']->title }}</h1>
        <div class="row">
            <div class="col s4 full_w" style="padding-left:0;">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><i aria-hidden="true" class="fa fa-users "></i><b>Élèves inscrits</b></div>
                        <div class="collapsible-body wrapenfents">
                            <ul class="enfants trans">
                            @foreach($data['eleves'] as $eleve)
                                <a href="#" class="enfant"
                                    @click.prevent="
                                        showEleveNotesBox = true;
                                        bullSearch.cour='{{$data['cour']->id}}'; 
                                        bullSearch.from='{{$data['user']->id}}'; 
                                        bullSearch.to='{{$eleve->id}}';
                                        bullSearch.toname='{{ $eleve->name.' '.$eleve->lname }}';
                                        showAddNoteBox = false;
                                        getBulletin();" >
                                    <div class="circle responsive-img imgfullbkg" style="background-image: url({{ getAvatar($eleve->id) }}); width:60px; height:60px; "></div>
                                    <div class="infos tooltipped" data-position="right" data-delay="5" data-tooltip="Cliquer pour ajouter une note">
                                        <span style="color:#777d84;">
                                            <i class="fa fa-circle" style="color: @if ($eleve->active) #00c853 @else #b3b3b3 @endif ;"  aria-hidden="true"></i> 
                                            <b>{{$eleve->matricule}}</b>
                                        </span>
                                        <p>{{ $eleve->name.' '.$eleve->lname }}</p>
                                    </div>
                                    <div class="clear"></div>
                                </a>
                            @endforeach
                            </ul>
                            @if (count($data['eleves']) < 1) <p style="padding:0 10px;">Aucun élève inscrit dans la classe <b>{{$data['classe']->code.' - '.$data['classe']->nom}}</b></p> @endif
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i aria-hidden="true" class="fa fa-folder-open "></i><b>Détails du cours</b></div>
                        <div class="collapsible-body">
                            <table class="bordered">
                                <tbody>
                                    @php $nbInscrits = count($data['eleves']); @endphp
                                    <tr>
                                        <td><b>Classe</b></td>
                                        <td class="r"><b>{{ $data['classe']->code }}</b> {{ $data['classe']->nom }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Inscrits</b></td>
                                        <td class="r">{{$nbInscrits}}</td>
                                    </tr>
                                </tbody>
                            </table><br>
                            <b>Description du cour: </b>
                            <span>{{ $data['cour']->description }}</span>
                        </div>
                    </li>
                </ul>
            </div>
            <div v-show="showEleveNotesBox" class="col s8 full_w card elevenotes">
                <div class="bloc elnotes">
                    <h2>Notes de <b>@{{bullSearch.toname}}</b> - cours <b>{{ $data['cour']->code }}</b>
                        <span class="delelemform" v-if="showbtndelelem">
                            <span style="color:#ec4d4d; font-size:13px;">Retirer @{{elemtormv.name}} ?</span>
                            {!! Form::open(['url' => '#']) !!}
                                <input type="hidden" name="id" :value="elemtormv.id" >
                                <input @click.prevent="delNote()" type="submit" class="sub" value="ok">
                            {!! Form::close() !!}
                        </span>
                    </h2>
                    <table class="bordered highlight responsive-table notes digitable">
                        <thead>
                            <tr>
                            <th>Éval.</th>
                            <th>Note</th>
                            <th>Commentaires</th>
                            <th>Retirer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="note in eleveNotes">
                                <td>@{{note.nom}}</td>
                                <td>@{{note.note}}/100</td>
                                <td>
                                    <a :href="s_url+'/discussions?discid='+note.discussion_id" title="Commentaires">
                                        <i aria-hidden="true" style="font-size:24px;" class="fa fa-weixin green-text"></i></a>
                                </td>
                                <td>
                                    <a href="#" title="Retirer" class="delelemicon"
                                        @click.prevent="showbtndelelem=true; elemtormv.id=note.id; elemtormv.name=note.nom">
                                        <i class="fa fa-window-close" aria-hidden="true" style="font-size:20px;"></i></a>
                                </td>
                            </tr>
                            <tr class="moy">
                                <td><b>Moyenne du cours:</b></td>
                                <td><b>@{{ getNotesMoy(eleveNotes) }}</b>/100</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="bloc addnote">
                    <a @click.prevent="showAddNoteBox=!showAddNoteBox" style="float:right;" class="btn waves-effect waves-light"><i class="material-icons left">playlist_add</i>Ajouter une note</a>
                    <div class="clear"></div>
                    <div class="addNoteBox" v-show="showAddNoteBox">
                        {!! Form::open(['url' => '#', 'id' => "addnoteform"]) !!}
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="nom" type="text" class="validate" name="nom" value="" required>
                                <label for="nom">Nom de l'évaluation</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="note" type="number" class="validate" name="note" value="" required>
                                <label for="note">Note</label>
                            </div>
                        </div>
                        <input type="hidden" name="u_from" :value="bullSearch.from" >
                        <input type="hidden" name="u_to" :value="bullSearch.to" >
                        <input type="hidden" name="cour_id" :value="bullSearch.cour" >
                        <input type="hidden" name="classe_id" value="{{ $data['classe']->id }}" >
                        <div class="row">
                            <div class="input-field col" style="margin-top:0; width:170px;">
                                <button type="submit" class="btn btn-primary">ajouter <i class="material-icons right">send</i></button>
                            </div>
                            <div id="axios_output" onclick="this.innerHTML=null" class="col" style="margin-top:0; width:calc(100% - 170px);"></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="clear"></div>
</div>


@endsection