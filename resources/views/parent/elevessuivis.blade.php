@extends('layouts.beforesingle')

@section('beforesingle')
<?php 
    $user = $data['parent'];
    $profile = $user->profile;
 ?>

<div id="viewWrap" class="viewWrap admin_parent mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Suivis des parcours scolaire</h1>
        <div class="row">
            <div class="col s6 r full_w enfants" style="padding-left:0;">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><i aria-hidden="true" class="fa fa-users "></i><b>Enfant(s) suivis</b></div>
                        <div class="collapsible-body">
                            @foreach($data['enfants'] as $enfant)
                            <a href="{{ route('getDiscussions', ['euid' => $enfant->id]) }}" class="enfant trans2">
                                <div class="circle responsive-img imgfullbkg" style="background-image: url({{ getAvatar($enfant->id) }}); width:60px; height:60px; "></div>
                                <div class="infos">
                                    <span style="color:#777d84;">
                                        <i class="fa fa-circle" style="color: @if ($enfant->active) #00c853 @else #b3b3b3 @endif ;"  aria-hidden="true"></i> 
                                        <b>{{$enfant->profile->matricule}}</b>
                                    </span>
                                    <p><b>{{ $enfant->name.' '.$enfant->lname }}</b>, <i><?= ($enfant->profile->classe)? $enfant->profile->classe->nom : 'n/a' ?></i></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                            @endforeach
                            @if (count($data['enfants']) < 1) <p style="padding:0 10px;">Vous ne suivez encore aucun enfant</p> @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<!--  -->
	
	<div class="clear"></div>
</div>

@endsection