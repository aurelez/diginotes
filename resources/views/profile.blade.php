@extends('layouts.beforesingle')

@section('beforesingle')

<?php 
	$user = $data['user'];
	$profile = $user->profile;
 ?>
<div id="viewWrap" class="viewWrap d_profile  mainpad" data-loadhelper="">
	<div class="focused full_w">
		<h1><i class="fa fa-address-card" aria-hidden="true"></i> Votre profile: <span>{{ $user->name.' '.$user->lname }}</span></h1>
		<div class="fiche card full_w">
			<div class="row digiUfiche valign-wrapper">
				<div class="col">
                    <div class="circle responsive-img imgfullbkg" style="background-image: url({{ getAvatar(Auth::user()->id).'?x='.rand(0, 999) }}); width:115px; height:115px; border:1px solid #f1f1f1;"></div>
				</div>
				<div class="col">
					<h2>
						<b>Nom: </b>{{$user->name}}<br>
						<b>Prénom: </b>{{$user->lname}}
					</h2>
					<p>
						<b>Matricule: </b>{{$profile->matricule}}<br>
					</p>
				</div>
				<div>
					@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
						<a class="btn btn-floating pulse" ><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
				</div>
			</div>
			<div class="row userinfo" style="padding-bottom:0;">
				@if ($user->active) <p style="margin-left:10px;"><i class="fa fa-circle" style="color:#00c853;" aria-hidden="true"></i> Compte actif</p>
				@else <p style="margin-left:10px;"><i class="fa fa-circle" aria-hidden="true" style="color:#b3b3b3;"></i> Compte inactif</p> @endif
				<!--  -->
				<div class="addforms">
					{!! Form::open(['url' => route('updateProfile'), 'files'=> true]) !!}
		            <div class="row">
		                <div class="input-field col s6">
		                    <input id="name" type="text" class="validate" name="name" value="{{ $user->name }}" required>
		                    <label class="active" for="name">Nom</label>
		                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
		                </div>
		                <div class="input-field col s6">
		                    <input id="lname" type="text" class="validate" name="lname" value="{{ $user->lname }}" required>
		                    <label class="active" for="lname">Prénom</label>
		                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
		                </div>
		            </div>
		            <div class="row">
		                <div class="input-field col s6">
		                    <input id="email" type="email" class="validate" name="email" value="{{ $user->email }}" required>
		                    <label class="active" for="email">E-Mail Address</label>
		                    @if ($errors->has('email')) <span class="m_fail"><b>{{ old('email') }}</b>: {{ $errors->first('email') }}</span> @endif
		                </div>
		                <div class="input-field col s6">
		                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ $profile->birthday }}" required>
		                    <label class="active" for="birthday">Date de naissance</label>
		                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
		                </div>
		            </div>
		            <div class="row">
		                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
		                    <input id="password" type="text" class="validate" name="password" onfocus="this.type='password'">
		                    <label class="active" for="password">Password</label>
		                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
		                </div>
		                <div class="input-field col s6">
		                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" onfocus="this.type='password'">
		                    <label class="active" for="password-confirm">Confirmer le Password</label>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col s6">
			                <div class="col s6">
			                    <p>
			                        <input name="sexe" type="radio" id="m" value="M" @if ($profile->sexe === 'M') checked @endif>
			                        <label for="m">M</label>
			                    </p>
			                </div>
			                <div class="col s6">
			                    <p>
			                        <input name="sexe" type="radio" id="f" value="F" @if ($profile->sexe === 'F') checked @endif/>
			                        <label for="f">F</label>
			                    </p>
			                </div>
		                </div>
		            </div>
		            <div class="row">
						<div class="file-field input-field col s12">
							<div class="btn">
								<span>Photo</span>
								<input name="avatar" type="file">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text" placeholder="Mettre à jour la photo de profile">
							</div>
							@if ($errors->has('avatar')) <span class="m_fail">{{ $errors->first('avatar') }}</span> @endif
						</div>
					</div>
					<input type="hidden" name="uid" value="{{$user->id}}" >
		            <div class="row" style="text-align: right;">
		                <div class="input-field col s6 offset-s6"><br>
		                    <button type="submit" class="btn btn-primary">Envoyer <i class="material-icons right">send</i></button>
		                </div>
		            </div>
		            {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection