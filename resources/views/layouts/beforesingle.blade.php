@extends('layouts.default')

@section('content')

<div class="digi_with_side">
    <div class="digi_sidebar">
        <div class="lside">
            <a class='dropdown-button' href='#' data-activates='dropmenu'><i class="small material-icons">view_headline</i></a>
            <ul id='dropmenu' class='dropdown-content'>
                <li><a href="#!"><i class="fa fa-user" aria-hidden="true"></i>Profile</a>
                <li><a href="#!"><i class="fa fa-info-circle" aria-hidden="true"></i>Help</a></li></li><li class="divider"></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-toggle-off" aria-hidden="true"></i>Déconnexion</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
        <div class="digimenu">
            <!-- infos -->
            <div class="row valign-wrapper cuinfos">
                <div class="col s4">
                    <div class="circle responsive-img imgfullbkg" style="background-image: url({{ getAvatar(Auth::user()->id) }}); width:60px; height:60px; "></div>
                </div>
                <div class="col s8">
                    Bonjour {{ Auth::user()->name }}
                    <div><i class="fa fa-circle" aria-hidden="true"></i> {{ ucfirst(Auth::user()->role) }}</div>
                </div>
            </div>
            <!-- option -->
            <div class="digioptions">
                <ul>
                    <li><a href="{{ route('userProfile') }}" class="@if($data['activepage'] == 'profile') active @endif"><i class="fa fa-address-card" aria-hidden="true"></i> Profile utilisateur</a></li><br>
                    
                    @if(getAuthRole() == 'admin')
                    <li><a href="{{ route('adminEleves') }}" class="@if($data['activepage'] == 'eleves') active @endif"><i class="fa fa-users" aria-hidden="true"></i> Élèves</a></li>
                    <li><a href="{{ route('adminParents') }}" class="@if($data['activepage'] == 'parents') active @endif"><i class="fa fa-users" aria-hidden="true"></i> Parents</a></li>
                    <li><a href="{{ route('adminProfesseurs') }}" class="@if($data['activepage'] == 'professeurs')active @endif"><i class="fa fa-users" aria-hidden="true"></i> Enseignants</a></li><br>
                    <li><a href="{{ route('adminCours') }}" class="@if($data['activepage'] == 'cours') active @endif"><i class="fa fa-folder-open" aria-hidden="true"></i> Cours</a></li>
                    <li><a href="{{ route('adminClasses') }}" class="@if($data['activepage'] == 'classes') active @endif"><i class="fa fa-university" aria-hidden="true"></i> Classes</a></li>
                    <!-- <li><a href="#" class="@if($data['activepage'] == 'horaires') active @endif"><i class="fa fa-clock-o" aria-hidden="true"></i> Horaires</a></li>  -->
                    @endif

                    @if(getAuthRole() == 'professor')
                    <li><a href="{{ route('profCours') }}" class="@if($data['activepage'] == 'classes') active @endif"><i class="fa fa-folder-open" aria-hidden="true"></i> Mes cours</a></li>
                    @endif

                    @if(getAuthRole() == 'eleve')
                    <li><a href="{{ route('getBulletins') }}" class="@if( in_array($data['activepage'], ['bulletins']) ) active @endif"><i class="fa fa-folder-open" aria-hidden="true"></i> Mes bulletins</a></li>
                    @endif

                    @if( (getAuthRole() == 'professor') || (getAuthRole() == 'eleve') )
                    <li><a href="{{ route('getDiscussions') }}" class="@if($data['activepage'] == 'discussions') active @endif"><i class="fa fa-weixin" aria-hidden="true"></i> Mes discussions</a></li>
                    @endif

                    @if(getAuthRole() == 'parent')
                    <li><a href="{{ route('getElevesSuivis') }}" class="@if( in_array($data['activepage'], ['elevesSuivis', 'discussions']) ) active @endif"><i class="fa fa-users" aria-hidden="true"></i> Enfants suivis</a></li>
                    <li><a href="{{ route('getEnfantsBulletins') }}" class="@if( in_array($data['activepage'], ['bulletins']) ) active @endif"><i class="fa fa-folder-open" aria-hidden="true"></i> Archive bulletins</a></li>
                    @endif

                    <br>
                    <li><a href="https://etudier.uqam.ca/cours?sigle=INM5151" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i> Besoin d'aide ?</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="app_right">
        <div class="bigcontainer">
            @include('layouts.navbar')
            @yield('beforesingle')
        </div>
        @include('layouts.footer')
    </div>
</div>

@endsection