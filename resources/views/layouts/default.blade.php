<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="MobileOptimized" content="320">
	<meta http-equiv="cleartype" content="on">
	<meta name="HandheldFriendly" content="True">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	{{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css') }}
	{{ Html::style('https://fonts.googleapis.com/icon?family=Material+Icons') }}
	{{ Html::style('http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}
	{{ Html::style('css/app.css') }}
	<title>{{$data['title']}}</title>
</head>
<body>
	<script>window.s_url = "{{ URL::to('/') }}";</script>
	<main id="digiapp" class="maincontainer {{ !empty($data['route']) ? $data['route'] : ''}}">
		@yield('content')
	</main>
</body>
{{-- Html::script('https://unpkg.com/vue') --}}
{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js') }}
{{ Html::script('js/app.js') }}
{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js') }}
</html>
