<div class="digi_navbar navbar-fixed">
    <nav class="app_nav">
        <div class="nav-wrapper">
            <a class="brand-logo" href="{{ url('/') }}"><img src="{{ asset('images/logo.svg') }}" alt="logo"></a>
            <ul class="right hide-on-med-and-down">
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">S'inscrire</a></li>
                @else
                    <li><a href="{{ route('userProfile') }}">Profile</a></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Déconnexion</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
                <li><a href="https://etudier.uqam.ca/cours?sigle=INM5151" target="blank">INM5151</a></li>
            </ul>
        </div>
    </nav>
</div>