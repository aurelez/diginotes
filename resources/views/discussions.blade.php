@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_classe prof_cour digi_discussions mainpad" data-loadhelper="" data-targdiscid="{{$data['targDiscId']}}">
	<div class="focused full_w">
        <h1><i class="fa fa-wechat" aria-hidden="true"></i> Discussions @if($data['user']->role === 'parent') à propos de <b>{{getUsername($data['euid'])}}</b> @endif</h1>
        <div class="row wrapdiscbox">
            <div class="col s5 full_w" style="padding-left:0;">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><b>Liste des conversations</b></div>
                        <div class="collapsible-body wrapenfents">
                            <ul class="enfants trans">
                            @foreach($data['discussions'] as $discussion)
                                @php $uTo = $data['isProf'] ? $discussion->u_to : $discussion->u_from @endphp
                                <a href="#" class="enfant disc_{{$discussion->discussion_id}}"
                                    @click.prevent="
                                        showWrapBox = true;
                                        discussion_id = {{$discussion->discussion_id}};
                                        getComments();
                                    " >
                                    <div class="infos">
                                        <span style="color:#777d84;">
                                            <i class="fa fa-comments" aria-hidden="true"></i> 
                                            <b>{{ getUsername($uTo) }}</b>
                                            {{$discussion->note_nom}}:  {{$discussion->classe_code}} - {{$discussion->cour_code}} - <b>{{$discussion->note}}</b>/100 
                                        </span>
                                    </div>
                                    <div class="clear"></div>
                                </a>
                            @endforeach
                            </ul>
                            @if (count($data['discussions']) < 1) <p style="padding:0 10px;">Aucune discussion trouvée</p> @endif
                        </div>
                    </li>
                </ul>
            </div>
            <div v-show="showWrapBox" class="col s7 full_w card comments">
                <div class="dtitle"></div>

                <div class="bloc comment" v-for="comment in comments">
                    <div class="circle responsive-img imgfullbkg" :style="{ 'background-image': 'url(' +s_url + '/storage/avatars/' +(comment.hasimg ? comment.hasimg : 'user-default.svg')+ ')' }"></div>
                    <div class="content">
                        <h5>@{{comment.name+' '+comment.lname}} <span>(@{{comment.role}})</span></h5>
                        <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> @{{comment.updated_at.substring(0, 10)}}</span>
                        <p>@{{comment.content}}</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <p v-if="comments.length == 0" class="orange-text darken-4" style="padding:0; margin:0 0 10px 0;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aucun commentaire trouvé!</p>
                <div v-if="showSpinner" style="width:100%; text-align:center; margin-bottom:5px; color:#34495e;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>

                <div class="bloc addnote">
                    <div class="addNoteBox">
                        {!! Form::open(['url' => '#', 'id' => "addcommentform"]) !!}
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">mode_edit</i>
                                <textarea id="content" name="content" class="materialize-textarea" required></textarea>
                                <label class="active" for="content">Ajouter un commentaire</label>
                            </div>
                        </div>
                        <input type="hidden" name="user_id" value="{{$data['user']->id}}" >
                        <input type="hidden" name="discussion_id" :value="discussion_id" >
                        <div class="row" style="margin-bottom:5px;">
                            <div class="input-field col" style="float:right; margin-top:0;">
                                <button type="submit" class="btn btn-primary">Commnenter</button>
                            </div>
                            <div id="axios_output" onclick="this.innerHTML=null" class="col" style="margin-top:0; width:calc(100% - 170px);"></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="clear"></div>
</div>

@endsection