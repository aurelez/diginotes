@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_classe mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Détails de la classe: <b>{{ $data['classe']->code }}</b></h1>
        <div class="row">
            <div class="col s7 l full_w fiche card">
                <table class="bordered">
                    <tbody>
                        @php $nbInscrits = count($data['eleves']); $dispo = $data['classe']->capacite - $nbInscrits; @endphp
                        <tr>
                            <td><b>Code de la classe</b></td>
                            <td class="r">{{ $data['classe']->code }}</td>
                        </tr>
                        <tr>
                            <td><b>Nom de la classe</b></td>
                            <td class="r">{{ $data['classe']->nom }}</td>
                        </tr>
                        <tr>
                            <td><b>Capacité</b></td>
                            <td class="r">{{ $data['classe']->capacite }}</td>
                        </tr>
                        <tr>
                            <td><b>Inscrits</b></td>
                            <td class="r">{{$nbInscrits}}</td>
                        </tr>
                        <tr>
                            <td><b>Disponibilité</b></td>
                            <td class="r"><b>{{$dispo}}</b> places disponibles </td>
                        </tr>
                        <tr>
                            <td><b>Scolarité</b></td>
                            <td class="r"><b>{{ $data['classe']->frais_scolaire }}$</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col s5 r full_w">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><i aria-hidden="true" class="fa fa-folder-open "></i><b>Cours à suivre</b></div>
                        <div class="collapsible-body">
                            <ul>
                            @foreach($data['classe']->cour as $cour)
                                <li><i class="fa fa-check" aria-hidden="true"></i> <b>{{$cour->code}}</b> {{$cour->title}}</li>
                            @endforeach
                            </ul>
                            @if (count($data['classe']->cour) < 1) <p style="padding:0 10px;">Aucun cours encore associé!</p> @endif
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i aria-hidden="true" class="fa fa-users "></i><b>Élèves actuellement inscrits</b></div>
                        <div class="collapsible-body">
                            <ul>
                            @foreach($data['eleves'] as $eleve)
                                <li><i class="fa fa-check" aria-hidden="true"></i> {{$eleve->name}} {{$eleve->lname}}</li>
                            @endforeach
                            </ul>
                            @if (count($data['eleves']) < 1) <p style="padding:0 10px;">Aucun élève inscrit dans la classe <b>{{$data['classe']->code.' - '.$data['classe']->nom}}</b></p> @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-pencil" aria-hidden="true"></i> Modifier la classe <b>{{ $data['classe']->code }}</b></h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminUpdateClasse')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="code" type="text" class="validate" name="code" value="{{ $data['classe']->code }}" required>
                    <label class="active" for="code">Code</label>
                    @if ($errors->has('code')) <span class="m_fail">{{ $errors->first('code') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="nom" type="text" class="validate" name="nom" value="{{ $data['classe']->nom }}" required>
                    <label class="active" for="nom">Nom</label>
                    @if ($errors->has('nom')) <span class="m_fail">{{ $errors->first('nom') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="capacite" type="number" class="validate" name="capacite" value="{{ $data['classe']->capacite }}" required>
                    <label class="active" for="capacite">Capacite</label>
                    @if ($errors->has('capacite')) <span class="m_fail">{{ $errors->first('capacite') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="frais_scolaire" type="number" class="validate" name="frais_scolaire" value="{{ $data['classe']->frais_scolaire }}" required>
                    <label class="active" for="frais_scolaire">Frais scolaire</label>
                    @if ($errors->has('frais_scolaire')) <span class="m_fail">{{ $errors->first('frais_scolaire') }}</span> @endif
                </div>
            </div>
            <input type="hidden" name="id" value="{{$data['classe']->id}}" >
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Envoyer <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection