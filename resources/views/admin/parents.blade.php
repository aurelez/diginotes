@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_parents mainpad" data-loadhelper="">
	<div class="focused full_w">
		<h1><i class="fa fa-folder-open" aria-hidden="true"></i> Gestion des Parents <span>(@{{ computedParentsList.length }})</span></h1>
		<div class="searchfield input-field">
	        <input id="search" type="text" name="search" v-model="parentsSearch" autocomplete="off">
	        <label for="search"><i class="fa fa-search" aria-hidden="true"></i> Rechercher un parent</label>
	    </div>
		<div class="clear"></div>
		<div class="cours">
			<div class="digi_collection">
                <transition-group name="staggered-fade"
                    tag="ul"
                    v-bind:css="false"
                    v-on:before-enter="beforeEnter"
                    v-on:enter="enter"
                    v-on:leave="leave" >
                    <li v-for="(parent, index) in computedParentsList"
                        v-bind:key="parent.id" v-bind:data-index="index">
                        <a :href="'parent/'+parent.id">
                            <span class="fa-stack fa-sm" v-bind:style="{color: (parent.active == '1') ? '#00c853' : '#adadad'}">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                            </span>
                            @{{ parent.name +' '+ parent.lname }} <b v-if="parent.active == '1'"> (Actif)</b>
                        </a>
                    </li>
                </transition-group>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-plus" aria-hidden="true"></i> Ajouter un nouveau cours</h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminPostParent')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}" required>
                    <label for="name">Nom</label>
                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="lname" type="text" class="validate" name="lname" value="{{ old('lname') }}" required>
                    <label for="lname">Prénom</label>
                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s16">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                    <label for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail">{{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ old('birthday') }}" required>
                    <label for="birthday">Date de naissance</label>
                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" required onfocus="this.type='password'">
                    <label for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" required onfocus="this.type='password'">
                    <label for="password-confirm">Confirmer le Password</label>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="col s6">
                        <p>
                            <input name="sexe" type="radio" id="m" value="M" />
                            <label for="m">M</label>
                        </p>
                    </div>
                    <div class="col s6">
                        <p>
                            <input name="sexe" type="radio" id="f" value="F" />
                            <label for="f">F</label>
                        </p>
                    </div>
                </div>
                <div class="col s6">
                    <p><div class="switch">
                        <label>
                            Inactif
                            <input name="active" value="1" type="checkbox">
                            <span class="lever"></span>
                            Actif
                        </label>
                    </div></p>
                </div>
            </div>
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Ajouter <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection