@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_eleves mainpad" data-loadhelper="eleves">
	<div class="focused full_w">
		<h1><i class="fa fa-users" aria-hidden="true"></i> Gestion des élèves <span>(@{{ elevesLisit.length }})</span></h1>
		<div class="searchfield input-field">
	        <input id="search" type="text" @keyup="fetchEleves()" name="search" v-model="eleveSearch" autocomplete="off">
	        <label for="search"><i class="fa fa-search" aria-hidden="true"></i> Rechercher un élève</label>
	    </div>
		<div class="ucards">
			<!-- <transition-group name="fade"> -->
			<div class="card sticky-action" v-for="user in elevesLisit"> <!-- v-bind:key="user" -->
				<a :href="'eleve/'+user.user_id" class="card-image waves-effect waves-block waves-light imgfullbkg" 
					v-bind:style="{ 'background-image': user.hasimg ? 'url(' +s_url+ '/storage/avatars/' +user.hasimg+ ')' : 'url(' +s_url+ '/storage/avatars/user-default.svg' + ')' }"></a>
				<div class="card-content">
					<span class="card-title activator grey-text text-darken-4" >@{{user.lname+' '+user.name}}<i class="material-icons right">more_vert</i></span>
					<p v-if="user.active == '1'"><i class="fa fa-circle" style="color:#00c853;" aria-hidden="true"></i> Compte actif</p>
					<p v-else><i class="fa fa-circle" aria-hidden="true" style="color:#b3b3b3;"></i> Compte inactif</p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">@{{user.lname+' '+user.name}}<i class="material-icons right">close</i></span>
					<p>
						<b>Matricule: </b>@{{user.matricule}}<br>
						<b>Date de Naiss.: </b>@{{user.birthday}}<br>
						<b>Classe: </b>@{{user.classe}}<br>
						<b>Sex: </b> @{{user.sexe}}
					</p>
				</div>
			</div>
			<!-- </transition-group> -->
			<div class="clear"></div>
			<p v-if="elevesLisit.length == 0" class="orange-text darken-4" style="padding:0; margin:0 0 10px 0;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aucun résultat trouvé!</p>
		</div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-user-plus" aria-hidden="true"></i> Ajouter un nouvel éléve</h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminPostEleve')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}" required>
                    <label for="name">Nom</label>
                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="lname" type="text" class="validate" name="lname" value="{{ old('lname') }}" required>
                    <label for="lname">Prénom</label>
                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                    <label for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail">{{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ old('birthday') }}" required>
                    <label for="birthday">Date de naissance</label>
                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" required onfocus="this.type='password'">
                    <label for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" required onfocus="this.type='password'">
                    <label for="password-confirm">Confirmer le Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                	<select name="classe_id"><option value="" disabled selected required>Attribuer une classe</option>
                		@foreach ($data['classes'] as $classe)
							<option value="{{$classe->id}}">{{$classe->nom}}</option>
						@endforeach
					</select>
                </div>
                <div class="input-field col s6">
                	<select name="parent_id"><option value="" disabled selected>Attribuer un parent</option>
                		@foreach ($data['parents'] as $parent)
							<option value="{{$parent->id}}">{{$parent->name.' '.$parent->lname}}</option>
						@endforeach
					</select>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
	                <div class="col s6">
	                    <p>
	                        <input name="sexe" type="radio" id="m" value="M" />
	                        <label for="m">M</label>
	                    </p>
	                </div>
	                <div class="col s6">
	                    <p>
	                        <input name="sexe" type="radio" id="f" value="F" />
	                        <label for="f">F</label>
	                    </p>
	                </div>
                </div>
                <div class="col s6">
					<p><div class="switch">
						<label>
							Inactif
							<input name="active" value="1" type="checkbox">
							<span class="lever"></span>
							Actif
						</label>
					</div></p>
                </div>
            </div>
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Ajouter <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection