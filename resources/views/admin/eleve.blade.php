@extends('layouts.beforesingle')

@section('beforesingle')

<?php 
	$user = $data['user'];
	$profile = $user->profile;
	$bulletins = $user->bulletin;
 ?>
<div id="viewWrap" class="viewWrap admin_eleve mainpad" data-loadhelper="">
	<div class="focused full_w">
		<h1><i class="fa fa-address-card" aria-hidden="true"></i> Fiche scolaire: <span>{{ $user->name.' '.$user->lname }}</span></h1>
		<div class="fiche card full_w">
			<div class="row hd valign-wrapper" style="margin-bottom:0;">
				<div class="col s4">
                    <div class="circle responsive-img imgfullbkg" style="background-image: url( {{ getAvatar($user->id).'?x='.rand(0, 999) }} ); width:115px; height:115px; border:1px solid #f1f1f1;"></div>
				</div>
				<div class="col s8">
					<h2>
						<b>Nom: </b>{{$user->name}}<br>
						<b>Prénom: </b>{{$user->lname}}
					</h2>
					<p>
						<b>Matricule: </b>{{$profile->matricule}}<br>
					</p>
				</div>
			</div>
			<div class="row userinfo" style="padding-bottom:0;">
				@if ($user->active) <p><i class="fa fa-circle" style="color:#00c853;" aria-hidden="true"></i> Compte actif</p>
				@else <p><i class="fa fa-circle" aria-hidden="true" style="color:#b3b3b3;"></i> Compte inactif</p> @endif
				<table class="digitable bordered">
					<tbody>
						<tr>
							<td>Classe</td>
							<td class="r"><?= ($profile->classe)? $profile->classe->code.' '.$profile->classe->nom : 'n/a' ?></td>
						</tr>
						<tr>
							<td>Courriel</td>
							<td class="r">{{$user->email}}</td>
						</tr>
						<tr>
							<td>Date de Naissance</td>
							<td class="r">{{$profile->birthday}}</td>
						</tr>
						<tr>
							<td>Date d'inscription</td>
							<td class="r">{{$profile->created_at}}</td>
						</tr>
						<tr>
							<td>Sexe</td>
							<td class="r">{{$profile->sexe}}</td>
						</tr>
						<tr>
							<td>Parent de l'élève</td>
							<td class="r"><?= ($data['parent'])? $data['parent']->name . ' ' . $data['parent']->lname : 'n/a' ?></td>
						</tr>
					</tbody>
				</table>
			</div>
            <ul class="collapsible addbuletin" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header trans2 @if (session('status_bulletin') || !$errors->isEmpty()) active @endif"><i aria-hidden="true" class="fa fa-file-text"></i><b>Ajouter un bulletin</b></div>
                    <div class="collapsible-body">
                    	@if (session('status_bulletin'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
						<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status_bulletin') !!}</span>@endif
						<div class="addforms">
							{!! Form::open(['url' => route('adminPostBulletin')]) !!}
				            <div class="row">
				                <div class="input-field col s6">
				                	<select name="annee"><option value="" disabled selected required>Année</option>
				                		@for ($i = 2012; $i < date('Y', strtotime('+1 year')); $i++)
											<option value="{{$i}}">{{$i}}</option>
										@endfor
									</select>
                    				@if ($errors->has('unique_annee')) <span class="m_fail">{{ $errors->first('unique_annee') }}</span> @endif
                    				@if ($errors->has('annee')) <span class="m_fail">{{ $errors->first('annee') }}</span> @endif
				                </div>
				                <div class="input-field col s6">
				                	<select name="classe_id"><option value="" disabled selected required>Classe</option>
				                		@foreach ($data['classes'] as $classe)
											<option value="{{$classe->id}}">{{$classe->nom}}</option>
										@endforeach
									</select>
                    				@if ($errors->has('classe_id')) <span class="m_fail">{{ $errors->first('classe_id') }}</span> @endif
				                </div>
				            </div>
						    <input type="hidden" name="user_id" value="{{$user->id}}" >
						    <div class="row" style="text-align: right;">
						        <div class="input-field col s6 offset-s6"><br>
						            <button type="submit" class="btn btn-primary">Ajouter <i class="material-icons right">send</i></button>
						        </div>
						    </div>
						    {!! Form::close() !!}
						</div>
                    </div>
                </li>
            </ul>
		</div>
		<!-- -->
		<div class="additional full_w">
            <ul class="collapsible" data-collapsible="accordion">
                <li class="u_bulletins">
                    <div class="collapsible-header active"><i aria-hidden="true" class="fa fa-folder-open "></i><b>Historique Bulletins</b></div>
                    <div class="collapsible-body">
                    	@if(count($bulletins) > 0)
						<div class="card-tabs">
							<ul class="tabs tabs-fixed-width">
		                        @foreach($bulletins as $bulletin)
		                        	<li class="tab"><a href="#a{{$bulletin->annee}}">{{$bulletin->annee}}</a></li>
		                        @endforeach
							</ul>
						</div>
						<div class="card-content lighten-4">
	                        @foreach($bulletins as $bulletin)
								<div id="a{{$bulletin->annee}}">
									<div class="top">
										<h2><i aria-hidden="true" class="fa fa-user"></i> {{$user->name.' '.$user->lname}}</h2>
										<p><b>Année scolaire:</b> {{$bulletin->annee}}</p>
										<p><b>Classe:</b> {{$bulletin->classe->code.' '.$bulletin->classe->nom}}</p>
									</div>
									<table class="bordered highlight responsive-table notes">
										<thead>
											<tr>
											<th>Cours</th>
											<th>Éval.</th>
											<th>Note</th>
											</tr>
										</thead>
										<tbody>
											@foreach($bulletin->note()->orderBy('cour_id')->get() as $note)
											<tr>
												<td>{!!getCoursName($note->cour_id)!!}</td>
												<td>{{$note->nom}}</td>
												<td>{{$note->note}}/100</td>
											</tr>
											@endforeach
											<tr class="moy">
												<td></td>
												<td><b>Moyenne:</b></td>
												<td><b>{{getMoyenne($bulletin->note()->get())}}</b>/100</td>
											</tr>
										</tbody>
									</table>
								</div>
	                        @endforeach
						</div>
						@else <p style="padding:0 10px;">Aucun bulletin disponible pour <b>{{$user->name.' '.$user->lname}}</b>, Veuillez en ajouter.</p> @endif
                    </div>
                </li>
            </ul>
		</div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-pencil" aria-hidden="true"></i> Mettre à jour <b>{{ $user->name.' '.$user->lname }}</b></h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminUpdateEleve'), 'files'=> true]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="name" type="text" class="validate" name="name" value="{{ $user->name }}" required>
                    <label class="active" for="name">Nom</label>
                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="lname" type="text" class="validate" name="lname" value="{{ $user->lname }}" required>
                    <label class="active" for="lname">Prénom</label>
                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="email" type="email" class="validate" name="email" value="{{ $user->email }}" required>
                    <label class="active" for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail"><b>{{ old('email') }}</b>: {{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ $profile->birthday }}" required>
                    <label class="active" for="birthday">Date de naissance</label>
                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" onfocus="this.type='password'">
                    <label class="active" for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" onfocus="this.type='password'">
                    <label class="active" for="password-confirm">Confirmer le Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                	<select name="classe_id"><option value="" <?= !$profile->classe ? 'selected' : ''?> disabled required>Attribuer une classe</option>
                		@foreach ($data['classes'] as $classe)
							<option value="{{$classe->id}}" <?= ( $profile->classe && ($profile->classe->id == $classe->id) ) ? 'selected' : '' ?>>{{$classe->nom}}</option>
						@endforeach
					</select>
                </div>
                <div class="input-field col s6">
                	<select name="parent_id"><option value="" <?= !$data['parent'] ? 'selected' : ''?> disabled>Attribuer un parent</option>
                		@foreach ($data['parents'] as $parent)
							<option value="{{$parent->id}}" <?= ( $data['parent'] && ($data['parent']->id == $parent->id) ) ? 'selected' : '' ?>>{{$parent->name.' '.$parent->lname}}</option>
						@endforeach
					</select>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
	                <div class="col s6">
	                    <p>
	                        <input name="sexe" type="radio" id="m" value="M" @if ($profile->sexe === 'M') checked @endif>
	                        <label for="m">M</label>
	                    </p>
	                </div>
	                <div class="col s6">
	                    <p>
	                        <input name="sexe" type="radio" id="f" value="F" @if ($profile->sexe === 'F') checked @endif/>
	                        <label for="f">F</label>
	                    </p>
	                </div>
                </div>
                <div class="col s6">
					<p><div class="switch">
						<label>
							Inactif
							<input name="active" value="1" type="checkbox" @if ($user->active) checked @endif>
							<span class="lever"></span>
							Actif
						</label>
					</div></p>
                </div>
            </div>
            <div class="row">
				<div class="file-field input-field col s12">
					<div class="btn">
						<span>Photo</span>
						<input name="avatar" type="file">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text" placeholder="Mettre à jour la photo de profile">
					</div>
					@if ($errors->has('avatar')) <span class="m_fail">{{ $errors->first('avatar') }}</span> @endif
				</div>
			</div>
			<input type="hidden" name="uid" value="{{$user->id}}" >
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Envoyer <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection