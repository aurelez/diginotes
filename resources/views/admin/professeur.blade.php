@extends('layouts.beforesingle')

@section('beforesingle')
<?php 
    $user = $data['prof'];
    $profile = $user->profile;
 ?>

<div id="viewWrap" class="viewWrap admin_prof admin_classe mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Détails de l'enseignant: <b>{{ $user->name.' '.$user->lname }}</b></h1>
        <div class="row">
            <div class="col s5 l full_w fiche card" style="padding:0;">
                <div class="digiUfiche valign-wrapper">
                    <div class="col">
                        <div class="circle responsive-img imgfullbkg" style="background-image: url( {{ getAvatar($user->id).'?x='.rand(0, 999) }} ); width:115px; height:115px; border:1px solid #f1f1f1;"></div>
                    </div>
                    <div class="col">
                        <h2>
                            <b>Nom: </b>{{$user->name}}<br>
                            <b>Prénom: </b>{{$user->lname}}
                        </h2>
                        <p>
                            <b>Matricule: </b>{{$profile->matricule}}<br>
                        </p>
                    </div>
                </div>
                <div style="padding: 0 10px;">
                    <table class="bordered">
                        <tbody>
                            <tr>
                                <td><b>Nom</b></td>
                                <td class="r">{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td><b>Prénom</b></td>
                                <td class="r">{{ $user->lname }}</td>
                            </tr>
                            <tr>
                                <td><b>Courriel</b></td>
                                <td class="r">{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td><b>Date de Naiss.</b></td>
                                <td class="r">{{ $profile->birthday }}</td>
                            </tr>
                            <tr>
                                <td><b>Nombre de cours</b></td>
                                <td class="r">{{ count($data['pcours']) }}</td>
                            </tr>
                            <tr>
                                <td><b>Sexe</b></td>
                                <td class="r">{{ $profile->sexe ? $profile->sexe : 'nd' }}</td>
                            </tr>
                            <tr>
                                <td><b>Actif</b></td>
                                <td class="r">{{ $user->active ? 'OUI' : 'NON' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s7 r full_w profcours">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active">
                            <i aria-hidden="true" class="fa fa-folder-open "></i><b>Cours enseignés </b>({{ count($data['pcours']) }})
                            <span class="delelemform" v-if="showbtndelelem">
                                <span style="color:#ec4d4d; font-size:13px;">Retirer @{{elemtormv.name}} ?</span>
                                {!! Form::open(['url' => route('adminRmvProfCour')]) !!}
                                    <input type="hidden" name="id" :value="elemtormv.id" >
                                    <input type="submit" style="margin-top:10px;" class="sub" value="ok">
                                {!! Form::close() !!}
                            </span>
                        </div>
                        <div class="collapsible-body">
                            <ul>
                            @foreach($data['pcours'] as $cour)
                                <li><i class="fa fa-check" aria-hidden="true"></i> <b>{{$cour->courcode}}</b> ({{$cour->classecode.'-'.$cour->classename}})
                                    <a href="" title="Retirer" class="delelemicon"
                                        @click.prevent="showbtndelelem=true; elemtormv.id='{{$cour->id}}'; elemtormv.name='{{$cour->courcode.'-'.$cour->classecode}}'">
                                        <i class="fa fa-window-close" aria-hidden="true"></i></a>
                                </li>
                            @endforeach
                            </ul>
                            @if (count($data['pcours']) < 1) <p style="margin:0;">Aucun cours encore associé!</p> @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-pencil" aria-hidden="true"></i> Mettre à jour <b>{{ $user->name.' '.$user->lname }}</b></h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminUpdateProfesseur'), 'files'=> true]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="name" type="text" class="validate" name="name" value="{{ $user->name }}" required>
                    <label class="active" for="name">Nom</label>
                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="lname" type="text" class="validate" name="lname" value="{{ $user->lname }}" required>
                    <label class="active" for="lname">Prénom</label>
                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="email" type="email" class="validate" name="email" value="{{ $user->email }}" required>
                    <label class="active" for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail"><b>{{ old('email') }}</b>: {{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ $profile->birthday }}" required>
                    <label class="active" for="birthday">Date de naissance</label>
                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <select name="pclasse">
                        <option value="" disabled selected>Choisir une classe</option>
                        @foreach ($data['classes'] as $classe)
                            <option value="{{$classe->id}}">{{$classe->nom}}</option>
                        @endforeach
                    </select>
                    <label>Classe</label>
                    @if ($errors->has('choose_classe')) <span class="m_fail">{{ $errors->first('choose_classe') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <select name="pcours[]" multiple>
                        <option value="" disabled selected>Choix cours</option>
                        @foreach ($data['cours'] as $cour)
                            <option value="{{$cour->id}}">{{$cour->code}}</option>
                        @endforeach
                    </select>
                    <label>Cours enseignés</label>
                    @if ($errors->has('unique_cour')) <span class="m_fail">{{ $errors->first('unique_cour') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" onfocus="this.type='password'">
                    <label class="active" for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" onfocus="this.type='password'">
                    <label class="active" for="password-confirm">Confirmer le Password</label>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="col s6">
                        <p>
                            <input name="sexe" type="radio" id="m" value="M" @if ($profile->sexe === 'M') checked @endif>
                            <label for="m">M</label>
                        </p>
                    </div>
                    <div class="col s6">
                        <p>
                            <input name="sexe" type="radio" id="f" value="F" @if ($profile->sexe === 'F') checked @endif/>
                            <label for="f">F</label>
                        </p>
                    </div>
                </div>
                <div class="col s6">
                    <p><div class="switch">
                        <label>
                            Inactif
                            <input name="active" value="1" type="checkbox" @if ($user->active) checked @endif>
                            <span class="lever"></span>
                            Actif
                        </label>
                    </div></p>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field col s12">
                    <div class="btn">
                        <span>Photo</span>
                        <input name="avatar" type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Mettre à jour la photo de profile">
                    </div>
                    @if ($errors->has('avatar')) <span class="m_fail">{{ $errors->first('avatar') }}</span> @endif
                </div>
            </div>
            <input type="hidden" name="uid" value="{{$user->id}}" >
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Envoyer <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection