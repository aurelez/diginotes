@extends('layouts.beforesingle')

@section('beforesingle')
<?php 
    $user = $data['parent'];
    $profile = $user->profile;
 ?>

<div id="viewWrap" class="viewWrap admin_parent mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Détails du parent: <b>{{ $user->name.' '.$user->lname }}</b></h1>
        <div class="row">
            <div class="col s6 l full_w fiche card" style="padding:0;">
                <div class="digiUfiche valign-wrapper">
                    <div class="col">
                        <div class="circle responsive-img imgfullbkg" style="background-image: url( {{ getAvatar($user->id).'?x='.rand(0, 999) }} ); width:115px; height:115px; border:1px solid #f1f1f1;"></div>
                    </div>
                    <div class="col">
                        <h2>
                            <b>Nom: </b>{{$user->name}}<br>
                            <b>Prénom: </b>{{$user->lname}}
                        </h2>
                        <p>
                            <b>Matricule: </b>{{$profile->matricule}}<br>
                        </p>
                    </div>
                </div>
                <div style="padding: 0 10px;">
                    <table class="bordered">
                        <tbody>
                            <tr>
                                <td><b>Nom</b></td>
                                <td class="r">{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td><b>Prénom</b></td>
                                <td class="r">{{ $user->lname }}</td>
                            </tr>
                            <tr>
                                <td><b>Courriel</b></td>
                                <td class="r">{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td><b>Date de Naiss.</b></td>
                                <td class="r">{{ $profile->birthday }}</td>
                            </tr>
                            <tr>
                                <td><b>Nombre d'enfants</b></td>
                                <td class="r">{{ count($data['enfants']) }}</td>
                            </tr>
                            <tr>
                                <td><b>Sexe</b></td>
                                <td class="r">{{ $profile->sexe ? $profile->sexe : 'nd' }}</td>
                            </tr>
                            <tr>
                                <td><b>Actif</b></td>
                                <td class="r">{{ $user->active ? 'OUI' : 'NON' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s6 r full_w enfants">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><i aria-hidden="true" class="fa fa-users "></i><b>Enfant(s) suivis</b></div>
                        <div class="collapsible-body">
                            @foreach($data['enfants'] as $enfant)
                            <a href="{{ route('adminEleve', $enfant->id) }}" class="enfant trans2">
                                <div class="circle responsive-img imgfullbkg" style="background-image: url({{ getAvatar($enfant->id) }}); width:60px; height:60px; "></div>
                                <div class="infos">
                                    <span style="color:#777d84;">
                                        <i class="fa fa-circle" style="color: @if ($enfant->active) #00c853 @else #b3b3b3 @endif ;"  aria-hidden="true"></i> 
                                        <b>{{$enfant->profile->matricule}}</b>
                                    </span>
                                    <p><b>{{ $enfant->name.' '.$enfant->lname }}</b>, <i><?= ($enfant->profile->classe)? $enfant->profile->classe->nom : 'n/a' ?></i></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                            @endforeach
                            @if (count($data['enfants']) < 1) <p style="padding:0 10px;"><b>{{$user->name.' '.$user->lname}}</b> ne suit encore aucun enfant</p> @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-pencil" aria-hidden="true"></i> Mettre à jour <b>{{ $user->name.' '.$user->lname }}</b></h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminUpdateParent'), 'files'=> true]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="name" type="text" class="validate" name="name" value="{{ $user->name }}" required>
                    <label class="active" for="name">Nom</label>
                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="lname" type="text" class="validate" name="lname" value="{{ $user->lname }}" required>
                    <label class="active" for="lname">Prénom</label>
                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="email" type="email" class="validate" name="email" value="{{ $user->email }}" required>
                    <label class="active" for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail"><b>{{ old('email') }}</b>: {{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ $profile->birthday }}" required>
                    <label class="active" for="birthday">Date de naissance</label>
                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" onfocus="this.type='password'">
                    <label class="active" for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" onfocus="this.type='password'">
                    <label class="active" for="password-confirm">Confirmer le Password</label>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="col s6">
                        <p>
                            <input name="sexe" type="radio" id="m" value="M" @if ($profile->sexe === 'M') checked @endif>
                            <label for="m">M</label>
                        </p>
                    </div>
                    <div class="col s6">
                        <p>
                            <input name="sexe" type="radio" id="f" value="F" @if ($profile->sexe === 'F') checked @endif/>
                            <label for="f">F</label>
                        </p>
                    </div>
                </div>
                <div class="col s6">
                    <p><div class="switch">
                        <label>
                            Inactif
                            <input name="active" value="1" type="checkbox" @if ($user->active) checked @endif>
                            <span class="lever"></span>
                            Actif
                        </label>
                    </div></p>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field col s12">
                    <div class="btn">
                        <span>Photo</span>
                        <input name="avatar" type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Mettre à jour la photo de profile">
                    </div>
                    @if ($errors->has('avatar')) <span class="m_fail">{{ $errors->first('avatar') }}</span> @endif
                </div>
            </div>
            <input type="hidden" name="uid" value="{{$user->id}}" >
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Envoyer <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection