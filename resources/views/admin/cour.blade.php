@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_cours mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-ticket" aria-hidden="true"></i> Détails du cours: <b>{{ $data['cour']->code }}</b></h1>
        <div class="fiche card">
            <div class="row">
                <table class="bordered">
                    <tbody>
                        <tr>
                            <td><b>Code du Cours</b></td>
                            <td class="r">{{ $data['cour']->code }}</td>
                        </tr>
                        <tr>
                            <td><b>Titre du cours</b></td>
                            <td class="r">{{ $data['cour']->title }}</td>
                        </tr>
                        <tr>
                            <td><b>Classe associée</b></td>
                            <td class="r"><?php foreach($data['classes'] as $classe){ if($classe->id == $data['cour']->classe_id){echo "<b>".$classe->code."</b> - ". $classe->nom; break;}  }?></td>
                        </tr>
                        <tr>
                            <td><b>Description</b></td>
                            <td class="r">{{ $data['cour']->description }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-pencil" aria-hidden="true"></i> Modifier le cours <b>{{ $data['cour']->code }}</b></h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminUpdateCour')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="code" type="text" class="validate" name="code" value="{{ $data['cour']->code }}" required>
                    <label class="active" for="code">Code</label>
                    @if ($errors->has('code')) <span class="m_fail">{{ $errors->first('code') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="title" type="text" class="validate" name="title" value="{{ $data['cour']->title }}" required>
                    <label class="active" for="title">Titre</label>
                    @if ($errors->has('title')) <span class="m_fail">{{ $errors->first('title') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                	<select name="classe_id"><option value="" <?= !$data['cour']->classe_id ? 'selected' : ''?> disabled required>Attribuer une classe</option>
                        @foreach ($data['classes'] as $classe)
                            <option value="{{$classe->id}}" <?= ($data['cour']->classe_id == $classe->id) ? 'selected' : '' ?>>{{$classe->nom}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                	<textarea id="description" class="materialize-textarea" name="description">{{ $data['cour']->description }}</textarea>
                    <label class="active" for="description">Description</label>
                    @if ($errors->has('description')) <span class="m_fail">{{ $errors->first('description') }}</span> @endif
                </div>
            </div>
            <input type="hidden" name="id" value="{{$data['cour']->id}}" >
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Envoyer <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection