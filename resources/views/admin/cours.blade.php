@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_cours mainpad" data-loadhelper="">
	<div class="focused full_w">
		<h1><i class="fa fa-folder-open" aria-hidden="true"></i> Gestion des cours <span>(@{{ computedCoursList.length }})</span></h1>
		<div class="searchfield input-field">
	        <input id="search" type="text" name="search" v-model="coursSearch" autocomplete="off">
	        <label for="search"><i class="fa fa-search" aria-hidden="true"></i> Rechercher un cours</label>
	    </div>
		<div class="clear"></div>
		<div class="cours">
			<div class="digi_collection">
                <transition-group name="staggered-fade"
                    tag="ul"
                    v-bind:css="false"
                    v-on:before-enter="beforeEnter"
                    v-on:enter="enter"
                    v-on:leave="leave" >
                    <li v-for="(cour, index) in computedCoursList"
                        v-bind:key="cour.id" v-bind:data-index="index">
                        <a :href="'cour/'+cour.id">
                            <b>@{{ cour.code }}</b> - @{{ cour.title }}
                        </a>
                    </li>
                </transition-group>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-plus" aria-hidden="true"></i> Ajouter un nouveau cours</h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminPostCour')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="code" type="text" class="validate" name="code" value="{{ old('code') }}" required>
                    <label for="code">Code</label>
                    @if ($errors->has('code')) <span class="m_fail">{{ $errors->first('code') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="title" type="text" class="validate" name="title" value="{{ old('title') }}" required>
                    <label for="title">Titre</label>
                    @if ($errors->has('title')) <span class="m_fail">{{ $errors->first('title') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                	<select name="classe_id"><option value="" disabled selected required>Associer une classe</option>
                		@foreach ($data['classes'] as $classe)
							<option value="{{$classe->id}}">{{$classe->nom}}</option>
						@endforeach
					</select>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                	<textarea id="description" class="materialize-textarea" name="description" value="{{ old('description') }}" required></textarea>
                    <label for="description">Description</label>
                    @if ($errors->has('description')) <span class="m_fail">{{ $errors->first('description') }}</span> @endif
                </div>
            </div>
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Ajouter <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection