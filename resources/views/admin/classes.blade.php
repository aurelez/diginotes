@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_cours mainpad" data-loadhelper="">
	<div class="focused full_w">
		<h1><i class="fa fa-folder-open" aria-hidden="true"></i> Gestion des classes <span>(@{{ computedClassesList.length }})</span></h1>
		<div class="searchfield input-field">
	        <input id="search" type="text" name="search" v-model="classesSearch" autocomplete="off">
	        <label for="search"><i class="fa fa-search" aria-hidden="true"></i> Rechercher une classe</label>
	    </div>
		<div class="clear"></div>
		<div class="cours">
			<div class="digi_collection">
                <transition-group name="staggered-fade"
                    tag="ul"
                    v-bind:css="false"
                    v-on:before-enter="beforeEnter"
                    v-on:enter="enter"
                    v-on:leave="leave" >
                    <li v-for="(classe, index) in computedClassesList"
                        v-bind:key="classe.id" v-bind:data-index="index">
                        <a :href="'classe/'+classe.id" style="min-width:250px;">
                            <b>@{{ classe.code }}</b> - @{{ classe.nom }} 
                            <span style="float:right;">Capacité: <b>@{{ classe.capacite }}</b></span>
                        </a>
                    </li>
                </transition-group>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="rightfix full_w">
		<h2><i class="fa fa-plus" aria-hidden="true"></i> Ajouter une nouvelle classe</h2>
		@if (session('status'))<span class="sess_msg" v-if="showSessMsg" v-on:click="showSessMsg=false">
			<a class="btn btn-floating pulse"><i class="material-icons">done</i></a> {!! session('status') !!}</span>@endif
		<div class="addforms">
			{!! Form::open(['url' => route('adminPostClasse')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="code" type="text" class="validate" name="code" value="{{ old('code') }}" required>
                    <label for="code">Code</label>
                    @if ($errors->has('code')) <span class="m_fail">{{ $errors->first('code') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="nom" type="text" class="validate" name="nom" value="{{ old('nom') }}" required>
                    <label for="nom">Nom</label>
                    @if ($errors->has('nom')) <span class="m_fail">{{ $errors->first('nom') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="capacite" type="number" class="validate" name="capacite" value="{{ old('capacite') }}" required>
                    <label for="capacite">Capacite</label>
                    @if ($errors->has('capacite')) <span class="m_fail">{{ $errors->first('capacite') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="frais_scolaire" type="number" class="validate" name="frais_scolaire" value="{{ old('frais_scolaire') }}" required>
                    <label for="frais_scolaire">Frais scolaire</label>
                    @if ($errors->has('frais_scolaire')) <span class="m_fail">{{ $errors->first('frais_scolaire') }}</span> @endif
                </div>
            </div>
            <div class="row" style="text-align: right;">
                <div class="input-field col s6 offset-s6"><br>
                    <button type="submit" class="btn btn-primary">Ajouter <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
	<div class="clear"></div>
</div>

@endsection