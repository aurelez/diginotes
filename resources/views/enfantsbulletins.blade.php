@extends('layouts.beforesingle')

@section('beforesingle')

<div id="viewWrap" class="viewWrap admin_classe prof_cour digi_discussions mainpad" data-loadhelper="">
	<div class="focused full_w">
        <h1><i class="fa fa-folder-open" aria-hidden="true"></i> Archive bulletins</h1>
        <div class="row wrapdiscbox">
            <div class="col s5 full_w" style="padding-left:0;">
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><b>Liste des bulletins de vos enfants</b></div>
                        <div class="collapsible-body wrapenfents">
                            <ul class="enfants trans">
                            @foreach($data['enfants'] as $enfant)
                                <a href="{{ route('getBulletins', ['euid' => $enfant->id]) }}" class="enfant">
                                    <div class="infos">
                                        <span style="color:#777d84;">
                                            <i class="fa fa-file-text" aria-hidden="true"></i> 
                                            <b>{{ getUsername($enfant->id) }}</b> 
                                            <span>({{ count($enfant->bulletin) }} bulletins)</span>
                                        </span>
                                    </div>
                                    <div class="clear"></div>
                                </a>
                            @endforeach
                            </ul>
                            @if (count($data['enfants']) < 1) <p style="padding:0 10px;">Aucun enfant trouvé</p> @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="clear"></div>
</div>

@endsection