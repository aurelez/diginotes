@extends('layouts.default')

@section('content')

@include('layouts.navbar')
<?php  $data = ['title' => "DigiNote Register"];  ?>
<div class="digi_full_wrap mainpad">
    <div class="site_w">
        <div class="digilogin registerform site_w collection">
            <div class="row" style="margin:0;">
                <div class="col s12"><h1>Diginote Pré - Inscription</h1></div>
            </div>
            {!! Form::open(['url' => route('register')]) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}" required>
                    <label for="name">Nom</label>
                    @if ($errors->has('name')) <span class="m_fail">{{ $errors->first('name') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="lname" type="text" class="validate" name="lname" value="{{ old('lname') }}" required>
                    <label for="lname">Prénom</label>
                    @if ($errors->has('lname')) <span class="m_fail">{{ $errors->first('lname') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                    <label for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail">{{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="birthday" type="date" class="validate datepicker" name="birthday" value="{{ old('birthday') }}" required>
                    <label for="birthday">Date de naissance</label>
                    @if ($errors->has('birthday')) <span class="m_fail">{{ $errors->first('birthday') }}</span> @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" required onfocus="this.type='password'">
                    <label for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field col s6">
                    <input id="password-confirm" type="text" class="validate" name="password_confirmation" required onfocus="this.type='password'">
                    <label for="password-confirm">Confirmer le Password</label>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <p><b>Votre Rôle:</b> Notez qu'il s'agit d'une pré-inscription, votre compte devra être validé par le registrariat pour qu'il soit actif.</p>
                    @if ($errors->has('role')) <span class="m_fail">{{ $errors->first('role') }}</span> @endif
                </div>
                <div class="col s4">
                    <p>
                        <input name="role" type="radio" id="eleve" value="eleve" />
                        <label for="eleve">Élève</label>
                    </p>
                </div>
                <div class="col s4">
                    <p>
                        <input name="role" type="radio" id="professor" value="professor" />
                        <label for="professor">Professeur</label>
                    </p>
                </div>
                <div class="col s4">
                    <p>
                        <input name="role" type="radio" id="parent" value="parent"  />
                        <label for="parent">Parent</label>
                    </p>
                </div>
                <div class="col s12">
                    <small></small>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12"><br>
                    <button type="submit" class="btn btn-primary">S'enregistrer <i class="material-icons right">send</i></button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('layouts.footer')

@endsection
