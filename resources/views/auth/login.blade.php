@extends('layouts.default')

@section('content')

@include('layouts.navbar')
<?php  $data = ['title' => "DigiNote Login"];  ?>
<div class="digi_full_wrap mainpad">
    <div class="site_w">
        <div class="centeriz"><img src="{{ asset('images/logo.svg') }}" alt="logo"></div>
        @if ($errors->has('notactive'))<div class="centeriz logerrormsg card-panel white-text amber darken-4"> {{ $errors->first('notactive') }} </div> @endif
        <div class="digilogin site_w collection">
            <h1>Diginote Login</h1>
            {!! Form::open(['url' => route('login')]) !!}
            <div class="row">
                <div class="input-field">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                    <label for="email">E-Mail Address</label>
                    @if ($errors->has('email')) <span class="m_fail">{{ $errors->first('email') }}</span> @endif
                </div>
                <div class="input-field {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="text" class="validate" name="password" required onfocus="this.type='password'">
                    <label for="password">Password</label>
                    @if ($errors->has('password')) <span class="m_fail">{{ $errors->first('password') }}</span> @endif
                </div>
                <div class="input-field">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                    <label for="remember">Remember Me</label>
                </div>
                <div class="input-field"><br>
                    <button type="submit" class="btn btn-primary">Login</button>
                    <a href="{{ route('password.request') }}"> Forgot Your Password?</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <p style="text-align: center">Pas encore inscrit? <a href="{{ route('register') }}">Inscrivez-vous</a></p>
    </div>
</div>
@include('layouts.footer')

@endsection
