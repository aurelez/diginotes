<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;

class BulletinController extends Controller{
    
    //-- Get eleve bulletins
    //---------------------------------------------------------------------------------------------------------------------
    public function bulletins(Request $request){

		$c_user = Auth::user();

		$user = $c_user;
		if($c_user->role === 'parent')
    		$user = User::find( $request->input('euid') );

        if(!$user || $user->role != 'eleve')
            return f_msg("Il n'existe aucun élève correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Bulletin",
            'activepage' => 'bulletins',
            'user' => $user,
        ];
        return view('bulletins', ['data' => $data]);
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- get enfantsBulletins
    //----------------------------------------------------------------------------------------------------------------------------------
    public function enfantsBulletins(Request $request){

    	$user = Auth::user();

    	$data = [
            'title' => "DigiNote Bulletins",
            'activepage' => 'bulletins',
            'user' => $user,
            'enfants' => User::select('users.id', 'name', 'lname', 'active', 'hasimg')
					    				->join('profiles', 'users.id', 'profiles.user_id')
					    				->where('profiles.parent_id', $user->id)->get(),
        ];
        return view('enfantsbulletins', ['data' => $data]);
    }
}
