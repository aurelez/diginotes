<?php

namespace App\Http\Controllers\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;

class ParentController extends Controller{
    
    //-- Get eleves suivis
    //---------------------------------------------------------------------------------------------------------------------
    public function elevesSuivis(){

        $parent = Auth::user();
        if(!$parent || $parent->role != 'parent')
            return f_msg("Il n'existe aucun parent correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'elevesSuivis',
            'parent' => $parent,
            'enfants' => User::select('users.id', 'name', 'lname', 'active', 'hasimg')
					    				->join('profiles', 'users.id', 'profiles.user_id')
					    				->where('profiles.parent_id', $parent->id)->get(),
        ];
        return view('parent/elevessuivis', ['data' => $data]);
    }
}
