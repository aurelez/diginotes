<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Discussion;
use App\Note;
use App\Bulletin;
use App\Classe;
use App\Cour;

class DiscussionController extends Controller{

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- get discussion
    //----------------------------------------------------------------------------------------------------------------------------------
    public function discussions(Request $request){

    	$user = Auth::user();

        if($user->role === 'eleve'){
            $u_target = 'u_to';
            $uid = $user->id;
        }

    	if($user->role === 'professor' || $user->role === 'admin'){
            $u_target = 'u_from';
            $uid = $user->id;
        }

        if($user->role === 'parent'){
            $u_target = 'u_to';
            $uid = $request->input('euid');
        }

		$discussions = DB::table('notes')->select('discussion_id', 'notes.nom as note_nom', 'note', 'classes.code as classe_code', 'cours.code as cour_code', 'u_to', 'u_from')
                        ->join('users', 'notes.u_to', 'users.id')
                        ->join('cours', 'notes.cour_id', 'cours.id')
                        ->join('bulletins', 'notes.bulletin_id', 'bulletins.id')
                        ->join('classes', 'bulletins.classe_id', 'classes.id')
                        ->where('notes.'.$u_target, $uid)
                        ->orderBy('notes.id', 'desc')->get();

        //dd($discussions);

    	$data = [
            'title' => "DigiNote Discussions",
            'activepage' => 'discussions',
            'user' => $user,
            'discussions' => $discussions,
            'isProf' => $user->role === 'professor',
            'targDiscId' => $request->input('discid'),
            'euid' => $request->input('euid'),
        ];
        return view('discussions', ['data' => $data]);
    }
}
