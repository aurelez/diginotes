<?php

namespace App\Http\Controllers\Prof;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Classe;
use App\Cour;

class CourController extends Controller{

    //-- List cours
    public function cours(){
        $data = [
            'title' => "DigiNote Enseignant",
            'activepage' => 'classes',
            'pcours' => DB::table('prof_infos')->select('prof_infos.id', 'cour_id', 'prof_infos.classe_id', 'classes.code as classecode', 'classes.nom as classename', 'cours.code as courcode', 'cours.title as courtitle' )
                            ->join('classes', 'prof_infos.classe_id', '=', 'classes.id')
                            ->join('cours', 'prof_infos.cour_id', '=', 'cours.id')
                            ->where('prof_infos.user_id', Auth::user()->id)->get(),
        ];

        return view('prof/cours', ['data' => $data]);
    }

    //-- Singlecour
    public function cour($classe_id, $cour_id){

        $classe = Classe::find($classe_id);
        $cour = Cour::find($cour_id);

        if(!$classe || !$cour)
            return f_msg("Il n'existe aucune donnée correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Enseignant",
            'activepage' => 'classes',
            'user' => Auth::user(),
            'classe' => $classe,
            'cour' => $cour,
            'eleves' => DB::table('users')->select('users.id', 'name', 'lname', 'active', 'matricule')
                                        ->join('profiles', 'users.id', '=', 'profiles.user_id')
                                        ->where('classe_id', $classe_id)->get(),
        ];
        return view('prof/cour', ['data' => $data]);
    }
}
