<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use JavaScript;
use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;

use AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;

class ClasseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    //---------------------------------------------------------------------------------------------------------------------
    //-- Classe list
    //---------------------------------------------------------------------------------------------------------------------
    public function classes(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'classes',
        ];

        JavaScript::put([
            'classes_fromBk' => Classe::all()
        ]);

        return view('admin/classes', ['data' => $data]);
    }

    //-- Add Classe
    //---------------------------------------------------------------------------------------------------------------------
    public function postClasse(Request $request){

        $data = $request->input();
        $validate = $this->classeValidator($data);
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());

        $classe = Classe::create([
            'code' => $data['code'],
            'nom' => $data['nom'],
            'capacite' => $data['capacite'],
            'frais_scolaire' => $data['frais_scolaire'],
        ]);


        return redirect()->back()->with([
	        'status' => "La classe <b>".$classe->code."</b> a été bien ajouté!"
	    ]);
    }

    //-- single Classe
    //---------------------------------------------------------------------------------------------------------------------
    public function classe($id){

        $classe = Classe::find($id);
        if(!$classe)
            return f_msg("Il n'existe aucune classe correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'classes',
            'classe' => $classe,
            'eleves' => DB::table('users')->select('users.id', 'name', 'lname')
					    				->join('profiles', 'users.id', '=', 'profiles.user_id')
					    				->where('classe_id', $id)->get(),
        ];
        return view('admin/classe', ['data' => $data]);
    }

    //-- Update Classe
    //---------------------------------------------------------------------------------------------------------------------
    public function updateClasse(Request $request){

		$data = $request->input();
	    $classe = Classe::find($data['id']);

	    //-- check if code is changed
	    if( $classe->code != $data['code'] ){
	        $validate = Validator::make($data, ['code' => 'required|string|min:3|max:255|unique:classes']);
	        if($validate->fails())
	            return redirect()->back()->withInput()->withErrors($validate->errors());
	    }

	    $classe->code = $data['code'];
	    $classe->nom = $data['nom'];
	    $classe->capacite = $data['capacite'];
	    $classe->frais_scolaire = $data['frais_scolaire'];

	    $classe->save();

        return redirect()->back()->with([
        	'status' => "La classe <b>".$classe->code."</b> a été bien modifié!"
        ]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---classeValidator
    private function classeValidator(array $data){

        return Validator::make($data, [
            'code' => 'required|string|min:3|max:255|unique:classes',
            'nom' => 'required|string|min:3|max:255',
            'capacite' => 'required|numeric',
            'frais_scolaire' => 'required|numeric',
        ]);
    }
}
