<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserActivation;

use JavaScript;
use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;

use AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class EleveController extends Controller
{
	use RegistersUsers;
    use Notifiable;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    //-- eleves list: Note: the list is geted by api from axios for the live search. see app/Http/digiapiController.php)
    //---------------------------------------------------------------------------------------------------------------------
    public function eleves(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'eleves',
            'classes' => Classe::all(),
            'parents' => DB::table('users')->select('id', 'name', 'lname')->where('role', 'parent')->get(),
        ];

        return view('admin/eleves', ['data' => $data]);
    }

    //-- postEleve (adding eleve to DB by admin)
    //---------------------------------------------------------------------------------------------------------------------
    public function postEleve(Request $request){
    	// adding user

    	$regRoute = 'App\Http\Controllers\Auth\RegisterController';
    	$data = array_add($request->input(), 'role', 'eleve');
    	$validate = app($regRoute)->Validator( $data );

        //-- validate
    	if($validate->fails()){
    		return redirect()->back()->withInput()->withErrors($validate->errors());
    	}

    	$user = app($regRoute)->create( $data );

    	//update user profile
    	$user->profile->sexe = $request->input('sexe');
    	if($request->input('classe_id'))
    		$user->profile->classe_id = $request->input('classe_id');
    	if($request->input('parent_id'))
    		$user->profile->parent_id = $request->input('parent_id');

    	//--active the user if needed
    	if($request->input('active'))
    		$user->active = 1;

    	$user->profile->save();
    	$user->save();

    	//dd($user, $user->profile);

    	return redirect()->back()->with([
            'status' => "<b>".$user->lname . ' ' . $user->name . "</b> a bien été inscrit!",
        ]);
    }

    //-- single eleve (Show single eleve infos)
    //---------------------------------------------------------------------------------------------------------------------
    public function eleve($uid){

        $user = User::find($uid);
        if(!$user || $user->role != 'eleve')
            return f_msg("Il n'existe aucun élève correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'eleves',
            'user' => $user,
            'parent' => User::find( $user->profile->parent_id ),
            'classes' => Classe::all(),
            'parents' => DB::table('users')->select('id', 'name', 'lname')->where('role', 'parent')->get(),
        ];
        return view('admin/eleve', ['data' => $data]);
    }

    //-- updateEleve (uptade a eleve info + adding photo)
    //---------------------------------------------------------------------------------------------------------------------
    public function updateEleve(Request $request){

        $data = $request->input();
        $user = User::find($data['uid']);

        //-- check if email is changed
        if( $user->email != $data['email'] ){
            $validate = $this->userValidator($data, 'email');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        if( strlen($request->input('password')) > 0 || strlen($request->input('password_confirmation')) > 0 ){
            $validate = $this->userValidator($data, 'password');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        //-- check others fields
        $validate = $this->userValidator($data, 'others');
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());


        //-- now just update the eleve infos
        $user->name = $data['name'];
        $user->lname = $data['lname'];
        $user->email = $data['email'];
        if( !empty($request->input('password')) ) $user->password = bcrypt($data['password']);
        $user->profile->birthday = $data['birthday'];
        if($request->input('classe_id')) $user->profile->classe_id = $data['classe_id'];
        if($request->input('parent_id')) $user->profile->parent_id = $data['parent_id'];
        if($request->input('sexe')) $user->profile->sexe = $data['sexe'];

        //-- Avatar
        if($request->file('avatar')){
            $file = storeAvatar($request->file('avatar'), $data['uid']);
            if($file['error']){
                return redirect()->back()->withInput()->withErrors($validate->errors()->add('avatar', $file['ret']));
            }
            $user->profile->hasimg = $file['ret'];
        }

        //-- update eleve satus
        if($request->input('active')){
            //send notification if its was inactive
            if($user->active == '0')
                $user->notify(new UserActivation($data));
            $user->active = 1;
        }else{$user->active = 0;}

        //-- and save them
        $user->profile->save();
        $user->save();

        //dd($data, $user, $user->profile);

        return redirect()->back()->with([
            'status' => "<b>".$user->profile->matricule."</b> a été mise à jour!"
        ]);
    }

    //-- Add Bulletin
    //---------------------------------------------------------------------------------------------------------------------
    public function postBulletin(Request $request){
		$data = $request->input();
		//dd($data);

        $validate = $this->bulletinValidator($data);
        if($validate->fails())
        	return redirect()->back()->withInput()->withErrors($validate->errors());

        //check if bulletin exist with the same year
        $temp_bul = DB::table('bulletins')->where('user_id', $data['user_id'])->where('annee', $data['annee'])->first();
        if($temp_bul){ 
            return redirect()->back()->withInput()
            ->withErrors($validate->errors()->add('unique_annee', "L'élève a déjà un bulletin pour ".$data['annee']));
        }

        $bulletin = Bulletin::create([
		    'user_id' => $data['user_id'],
		    'classe_id' => $data['classe_id'],
		    'annee' => $data['annee'],
		]);

        return redirect()->back()->with([
	    	'status_bulletin' => "Le bulletin a été ajouté!"
	    ]);
    }
    
    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---Conditional user userValidator
    private function userValidator(array $data, $target){
    
        if($target === 'email')
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
            ]);

        if($target === 'password')
            return Validator::make($data, [
                'password' => 'required|string|min:3|confirmed',
            ]);

        if($target === 'others')
            return Validator::make($data, [
                'name' => 'required|string|min:3|max:255',
                'lname' => 'required|string|min:3|max:255',
            ]);
    }

    //---bulletinValidator
    private function bulletinValidator(array $data){

        return Validator::make($data, [
            'user_id' => 'required|numeric',
            'classe_id' => 'required|numeric',
            'annee' => 'required|numeric',
        ]);
    }
}