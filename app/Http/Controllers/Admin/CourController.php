<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use JavaScript;
use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;

use AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class CourController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    //---------------------------------------------------------------------------------------------------------------------
    //-- Cours list
    //---------------------------------------------------------------------------------------------------------------------
    public function cours(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'cours',
            'classes' => Classe::all()
        ];

        JavaScript::put([ //https://github.com/laracasts/PHP-Vars-To-Js-Transformer
            'cours_fromBk' => Cour::all()
        ]);

        return view('admin/cours', ['data' => $data]);
    }

    //-- Add cour
    //---------------------------------------------------------------------------------------------------------------------
    public function postCour(Request $request){

        $data = $request->input();
        $validate = $this->courValidator($data);
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());

        $cour = Cour::create([
            'classe_id' => $data['classe_id'],
            'code' => $data['code'],
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return redirect()->back()->with([
	        'status' => "Le cours <b>".$cour->code."</b> a été bien ajouté!"
	    ]);
    }

    //-- single cour
    //---------------------------------------------------------------------------------------------------------------------
    public function cour($id){

        $cour = Cour::find($id);
        if(!$cour)
            return f_msg("Il n'existe aucun cours correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'cours',
            'cour' => $cour,
            'classes' => Classe::all(),
        ];
        return view('admin/cour', ['data' => $data]);
    }

    //-- Update cour
    //---------------------------------------------------------------------------------------------------------------------
    public function updateCour(Request $request){

		$data = $request->input();
	    $cour = Cour::find($data['id']);

	    //-- check if code is changed
	    if( $cour->code != $data['code'] ){
	        $validate = Validator::make($data, ['code' => 'required|string|min:3|max:255|unique:cours']);
	        if($validate->fails())
	            return redirect()->back()->withInput()->withErrors($validate->errors());
	    }

	    $cour->code = $data['code'];
	    $cour->title = $data['title'];
	    $cour->classe_id = $data['classe_id'];
	    $cour->description = $data['description'];

	    $cour->save();

        return redirect()->back()->with([
        	'status' => "Le cours <b>".$cour->code."</b> a été bien modifié!"
        ]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---courValidator
    private function courValidator(array $data){

        return Validator::make($data, [
            'code' => 'required|string|min:3|max:255|unique:cours',
            'title' => 'required|string|min:3|max:255',
            'classe_id' => 'required',
        ]);
    }
}
