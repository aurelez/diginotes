<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserActivation;

use JavaScript;
use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;

use AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class ParentController extends Controller
{
	use RegistersUsers;
    use Notifiable;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    //---------------------------------------------------------------------------------------------------------------------
    //-- Parents list
    //---------------------------------------------------------------------------------------------------------------------
    public function parents(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'parents',
        ];

        JavaScript::put([ //https://github.com/laracasts/PHP-Vars-To-Js-Transformer
            'parents_fromBk' => User::where('role', 'parent')->orderBy('users.id', 'desc')->get()
        ]);

        return view('admin/parents', ['data' => $data]);
    }

    //-- postParent by admin
    //---------------------------------------------------------------------------------------------------------------------
    public function postParent(Request $request){
    	// adding user

    	$regRoute = 'App\Http\Controllers\Auth\RegisterController';
    	$data = array_add($request->input(), 'role', 'parent');
    	$validate = app($regRoute)->Validator( $data );

        //-- validate
    	if($validate->fails()){
    		return redirect()->back()->withInput()->withErrors($validate->errors());
    	}

    	$user = app($regRoute)->create( $data );

    	//update user profile
    	$user->profile->sexe = $request->input('sexe');

    	//--active the user if needed
    	if($request->input('active'))
    		$user->active = 1;

    	$user->profile->save();
    	$user->save();

    	//dd($user, $user->profile);

    	return redirect()->back()->with([
            'status' => "<b>".$user->lname . ' ' . $user->name . "</b> a été ajouté!",
        ]);
    }

    //-- single parent
    //---------------------------------------------------------------------------------------------------------------------
    public function parent($uid){

        $parent = User::find($uid);
        if(!$parent || $parent->role != 'parent')
            return f_msg("Il n'existe aucun parent correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'parents',
            'parent' => $parent,
            'enfants' => User::select('users.id', 'name', 'lname', 'active', 'hasimg')
					    				->join('profiles', 'users.id', 'profiles.user_id')
					    				->where('profiles.parent_id', $uid)->get(),
        ];
        return view('admin/parent', ['data' => $data]);
    }

    //-- Update parent
    //---------------------------------------------------------------------------------------------------------------------
    public function updateParent(Request $request){

		$data = $request->input();
        $user = User::find($data['uid']);

        //-- check if email is changed
        if( $user->email != $data['email'] ){
            $validate = $this->userValidator($data, 'email');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        if( strlen($request->input('password')) > 0 || strlen($request->input('password_confirmation')) > 0 ){
            $validate = $this->userValidator($data, 'password');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        //-- check others fields
        $validate = $this->userValidator($data, 'others');
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());


        //-- now just update the user infos
        $user->name = $data['name'];
        $user->lname = $data['lname'];
        $user->email = $data['email'];
        if( !empty($request->input('password')) ) $user->password = bcrypt($data['password']);
        $user->profile->birthday = $data['birthday'];
        if($request->input('sexe')) $user->profile->sexe = $data['sexe'];

        //-- update user satus
        if($request->input('active')){
            //send notification if its was inactive
            if($user->active == '0')
                $user->notify(new UserActivation($data));
            $user->active = 1;
        }else{$user->active = 0;}

        //-- Avatar
        if($request->file('avatar')){
            $file = storeAvatar($request->file('avatar'), $data['uid']);
            if($file['error']){
                return redirect()->back()->withInput()->withErrors($validate->errors()->add('avatar', $file['ret']));
            }
            $user->profile->hasimg = $file['ret'];
        }

        //-- and save them
        $user->profile->save();
        $user->save();

        //dd($data, $user, $user->profile);

        return redirect()->back()->with([
            'status' => "<b>".$user->profile->matricule."</b> a été mise à jour!"
        ]);
    }


    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---Conditional user userValidator
    private function userValidator(array $data, $target){
    
        if($target === 'email')
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
            ]);

        if($target === 'password')
            return Validator::make($data, [
                'password' => 'required|string|min:3|confirmed',
            ]);

        if($target === 'others')
            return Validator::make($data, [
                'name' => 'required|string|min:3|max:255',
                'lname' => 'required|string|min:3|max:255',
            ]);
    }
}
