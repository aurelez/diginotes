<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserActivation;

use JavaScript;
use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;
use App\Prof_info;

use AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class ProfesseurController extends Controller
{
	use RegistersUsers;
    use Notifiable;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    //---------------------------------------------------------------------------------------------------------------------
    //-- Professeurs list
    //---------------------------------------------------------------------------------------------------------------------
    public function professeurs(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'professeurs',
            'classes' => Classe::all(),
            'cours' => Cour::all(),
        ];

        JavaScript::put([
            'profs_fromBk' => User::where('role', 'professor')->orderBy('users.id', 'desc')->get()
        ]);

        return view('admin/professeurs', ['data' => $data]);
    }

    //-- postProfesseur by admin
    //---------------------------------------------------------------------------------------------------------------------
    public function postProfesseur(Request $request){
    	// adding user

    	$regRoute = 'App\Http\Controllers\Auth\RegisterController';
    	$data = array_add($request->input(), 'role', 'professor');
    	$validate = app($regRoute)->Validator( $data );

        //-- validate
    	if($validate->fails()){
    		return redirect()->back()->withInput()->withErrors($validate->errors());
    	}

    	$user = app($regRoute)->create( $data );

    	//update user profile
    	$user->profile->sexe = $request->input('sexe');

    	//--active the user if needed
    	if($request->input('active'))
    		$user->active = 1;

    	$user->profile->save();
    	$user->save();

    	//-- add prof info
    	foreach ($request->input('pcours') as $cour_id) {
    		Prof_info::create([
	            'user_id' => $user->id,
	            'classe_id' => $request->input('pclasse'),
	            'cour_id' => $cour_id,
	        ]);
    	}

    	//dd($user, $user->profile);

    	return redirect()->back()->with([
            'status' => "<b>".$user->lname . ' ' . $user->name . "</b> a été ajouté!",
        ]);
    }

    //-- single professeur
    //---------------------------------------------------------------------------------------------------------------------
    public function professeur($uid){

        $prof = User::find($uid);
        if(!$prof || $prof->role != 'professor')
            return f_msg("Il n'existe aucun enseignant correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'professeurs',
            'prof' => $prof,
            'pcours' => DB::table('prof_infos')->select('prof_infos.id', 'classes.code as classecode', 'classes.nom as classename', 'cours.code as courcode', 'cours.title as courtitle' )
                            ->join('classes', 'prof_infos.classe_id', '=', 'classes.id')
                            ->join('cours', 'prof_infos.cour_id', '=', 'cours.id')
                            ->where('prof_infos.user_id', $uid)->get(),

            'classes' => Classe::all(),
            'cours' => Cour::all(),
        ];

        //dd($data);
        return view('admin/professeur', ['data' => $data]);
    }

    //-- Update prof
    //---------------------------------------------------------------------------------------------------------------------
    public function updateProfesseur(Request $request){

		$data = $request->input();
        $user = User::find($data['uid']);

        //-- check if email is changed
        if( $user->email != $data['email'] ){
            $validate = $this->userValidator($data, 'email');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        if( strlen($request->input('password')) > 0 || strlen($request->input('password_confirmation')) > 0 ){
            $validate = $this->userValidator($data, 'password');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        //-- check others fields
        $validate = $this->userValidator($data, 'others');
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());

        //-- put prof infos
        if($request->input('pcours')){
            foreach ($data['pcours'] as $cour_id) {

                if(!$request->input('pclasse')){ 
                    return redirect()->back()->withInput()
                    ->withErrors($validate->errors()->add('choose_classe', "Sélectionner d'abord une classe!"));
                }

                $temp_profinfo = DB::table('prof_infos')->where('user_id', $user->id)
                                    ->where('classe_id', $data['pclasse'])->where('cour_id', $cour_id)->first();

                if($temp_profinfo){
                    return redirect()->back()->withInput()->withErrors($validate->errors()
                        ->add('unique_cour', "Le prof enseigne déjà le cours ". Cour::find($cour_id)->code." dans la classe ". Classe::find($data['pclasse'])->code) );
                }

                Prof_info::create([
                    'user_id' => $user->id,
                    'classe_id' => $data['pclasse'],
                    'cour_id' => $cour_id,
                ]);
            }
        }

        //-- now just update the user infos
        $user->name = $data['name'];
        $user->lname = $data['lname'];
        $user->email = $data['email'];
        if( !empty($request->input('password')) ) $user->password = bcrypt($data['password']);
        $user->profile->birthday = $data['birthday'];
        if($request->input('sexe')) $user->profile->sexe = $data['sexe'];

        //-- update user satus
        if($request->input('active')){
            //send notification if its was inactive
            if($user->active == '0')
                $user->notify(new UserActivation($data));
            $user->active = 1;
        }else{$user->active = 0;}

        //-- Avatar
        if($request->file('avatar')){
            $file = storeAvatar($request->file('avatar'), $data['uid']);
            if($file['error']){
                return redirect()->back()->withInput()->withErrors($validate->errors()->add('avatar', $file['ret']));
            }
            $user->profile->hasimg = $file['ret'];
        }

        //-- and save them
        $user->profile->save();
        $user->save();

        //dd($data, $user, $user->profile);

        return redirect()->back()->with([
            'status_showrmvcouralert' => "<b>".$user->profile->matricule."</b> a été mise à jour!"
        ]);
    }

    //-- rmvProfCour
    //---------------------------------------------------------------------------------------------------------------------
    public function rmvProfCour(Request $request){
        DB::table('prof_infos')->where('id', $request->input('id'))->delete();
        return redirect()->back();
    }


    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---Conditional user userValidator
    private function userValidator(array $data, $target){
    
        if($target === 'email')
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
            ]);

        if($target === 'password')
            return Validator::make($data, [
                'password' => 'required|string|min:3|confirmed',
            ]);

        if($target === 'others')
            return Validator::make($data, [
                'name' => 'required|string|min:3|max:255',
                'lname' => 'required|string|min:3|max:255',
            ]);
    }
}
