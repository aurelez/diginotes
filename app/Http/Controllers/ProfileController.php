<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller{
    
    //----show profile
    //---------------------------------------------------------------------------------------------------------------------
    public function profile(){

        $data = [
            'title' => "DigiNote Profile",
            'activepage' => 'profile',
            'user' => Auth::user(),
        ];

        return view('profile', ['data' => $data]);
    }

    //-- updateEleve (uptade a eleve info + adding photo)
    //---------------------------------------------------------------------------------------------------------------------
    public function updateProfile(Request $request){

        $data = $request->input();
        $user = Auth::user();

        //-- check if email is changed
        if( $user->email != $data['email'] ){
            $validate = $this->userValidator($data, 'email');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        if( strlen($request->input('password')) > 0 || strlen($request->input('password_confirmation')) > 0 ){
            $validate = $this->userValidator($data, 'password');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        //-- check others fields
        $validate = $this->userValidator($data, 'others');
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());


        //-- now just update the user infos
        $user->name = $data['name'];
        $user->lname = $data['lname'];
        $user->email = $data['email'];
        if( !empty($request->input('password')) ) $user->password = bcrypt($data['password']);
        $user->profile->birthday = $data['birthday'];
        if($request->input('sexe')) $user->profile->sexe = $data['sexe'];

        //--Avatar
        if($request->file('avatar')){
            $file = storeAvatar($request->file('avatar'), $data['uid']);
            if($file['error']){
                return redirect()->back()->withInput()->withErrors($validate->errors()->add('avatar', $file['ret']));
            }
            $user->profile->hasimg = $file['ret'];
        }

        //-- and save them
        $user->profile->save();
        $user->save();

        //dd($data, $user, $user->profile);

        return redirect()->back()->with([
            'status' => "Votre profile a été mise à jour!"
        ]);
    }


    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---Conditional user userValidator
    private function userValidator(array $data, $target){
    
        if($target === 'email')
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
            ]);

        if($target === 'password')
            return Validator::make($data, [
                'password' => 'required|string|min:3|confirmed',
            ]);

        if($target === 'others')
            return Validator::make($data, [
                'name' => 'required|string|min:3|max:255',
                'lname' => 'required|string|min:3|max:255',
            ]);
    }
}
