<?php
//-- finaly use for conditional routing :( 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        // cause laravel redirectTo() not working propely in loginController (its framework bugg)
        if(Auth::check()){
            $user = Auth::user();

            if($user->active != 1){
                Auth::logout();
                return redirect('/login')->withErrors([
                    'notactive' => "Vous avez été inscrit. Votre Compte n'est pas encore activé.",
                ]);
            }
            return redirect()->route('userProfile');

            /*if ($user->role == 'admin') return redirect()->route('userProfile');
            if ($user->role == 'eleve') return '/eleve/profile';
            if ($user->role == 'parent') return '/parent/profile';*/
        }else{
            return redirect('/');
        }
    }
}
