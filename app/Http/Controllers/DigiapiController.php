<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Notifications\CommentNotify;
use App\Note;
use App\Bulletin;
use App\Discussion;
use App\Comment;
use App\User;

class DigiapiController extends Controller
{
    //----------------------------------------------------------------------------------------------------------------------------------
    //-- get eleve by search !!!! for prod use laravel passport to validate access
    //----------------------------------------------------------------------------------------------------------------------------------
    public function getEleves(Request $request){

        //if(!Auth::check()) return response()->json([ 'error' => 'Accesss denied' ]);
    	$users = DB::table('users')->select('user_id', 'name', 'lname', 'email', 'active', 'classes.nom as classe', 'matricule', 'birthday', 'sexe', 'infos', 'hasimg')
    				->join('profiles', 'users.id', '=', 'profiles.user_id')
    				->leftJoin('classes', 'profiles.classe_id', '=', 'classes.id') //yep! leftjoin is not join
    				->orderBy('users.id', 'desc')
    				->where('role', '=', 'eleve')
					->where( function($query) use ($request){
						$query->where('name', 'LIKE', "$request->search%")
							->orWhere('lname', 'LIKE', "$request->search%");
					})->paginate(60);

		return response()->json([
			'users'	=>	$users
		]);
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- get eleve notes
    //----------------------------------------------------------------------------------------------------------------------------------
    public function getNotes(Request $request){
        $notes = Note::where('cour_id', $request->cour)->where('u_from', $request->from)->where('u_to', $request->to)->get();

        return response()->json([
            'notes' =>  $notes
        ]);
        //test: http://localhost/diginotes/public/api/getNotes?cour=1&from=34&to=46
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- post eleve note
    //----------------------------------------------------------------------------------------------------------------------------------
    public function postNote(Request $request){

        //-- get the bulletin for this classe
        $bull = Bulletin::select('id')->where('user_id', $request->input('u_to'))->where('classe_id', $request->input('classe_id'))->first();

        if(!$bull){
            $u = User::find($request->input('u_to'));
            return response()->json(['ret' =>  "<span class='badge red'><b>".$u->name.' '.$u->lname."</b> n'a pas encore de bulletin pour cette classe, contactez l'admin. </span>" ]);
        }

        //validate
        $validate = $this->noteValidator($request->input());
            if($validate->fails())
                return response()->json([ 'ret' =>  "<span class='badge red'>Erreur dans les données envoyées!</span>" ]);

        //-- check if same name exist
        $tmpnote = Note::where('nom', $request->input('nom'))->where('cour_id', $request->input('cour_id'))->where('u_to', $request->input('u_to'))->where('u_from', $request->input('u_from'))->first();
        if($tmpnote) return response()->json([ 'ret' =>  "<span class='badge red'>Une évaluation existe déjà avec le même nom.</span>" ]);

        //-- create discussion
        $discussion = Discussion::create([
            'user_id' => $request->input('u_from'),
            'nom' => 'CL'.$request->input('classe_id').'CR'.$request->input('cour_id'),
        ]);

        //-- ok now, create note
        $note = Note::create([
            'nom' => $request->input('nom'),
            'cour_id' => $request->input('cour_id'),
            'u_from' => $request->input('u_from'),
            'u_to' => $request->input('u_to'),
            'note' => $request->input('note'),
            'bulletin_id' => $bull->id,
            'discussion_id' => $discussion->id,
        ]);

        $ret = $note ? "<span class='badge green'>La note a été ajoutée.</span>" : "<span class='badge red'>La note n'a pas pu être ajoutée, contactez l'admin.</span>"; 

        return response()->json([
            'ret' =>  $ret
        ]);
        //test on post: http://localhost:82/diginotes/public/api/postNote?classe_id=2&cour_id=1&u_from=34&u_to=46&nom=mini-test&note=90
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- delete eleve notes
    //----------------------------------------------------------------------------------------------------------------------------------
    public function delNote(Request $request){
        return response()->json([
            'ret' =>  DB::table('notes')->where('id', $request->input('id'))->delete() ? "ok!" : 'ko!'
        ]);
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- get comments
    //----------------------------------------------------------------------------------------------------------------------------------
    public function getComments(Request $request){

        $comments = DB::table('comments as c')->select('c.*', 'name', 'lname', 'role', 'hasimg')
                    ->join('users', 'c.user_id', 'users.id')
                    ->join('profiles', 'users.id', 'profiles.user_id')
                    ->where('c.discussion_id', $request->input('discid'))->get();

        return response()->json([
            'comments' =>  $comments
        ]);
        // test http://localhost:82/diginotes/public/api/getComments?disc_id=1
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //-- post comment
    //----------------------------------------------------------------------------------------------------------------------------------
    public function postComment(Request $request){

        $comment = Comment::create([
            'user_id' => $request->input('user_id'),
            'discussion_id' => $request->input('discussion_id'),
            'content' => $request->input('content'),
        ]);

        //--Notifyabonne---------------------------
        $abonnes = DB::table('notes')->select('users.id as abonne_id')
                    ->join('users', function ($join) {
                        $join->on('users.id', 'notes.u_from')->orOn('users.id', 'notes.u_to');
                    })->where('notes.discussion_id', $request->input('discussion_id'))->get();

        //--Get eleve parent id
        $parent = DB::table('users')->select('profiles.parent_id as abonne_id')
                    ->join('notes', 'users.id', 'notes.u_to')
                    ->join('profiles', 'profiles.user_id', 'users.id')
                    ->where('notes.discussion_id', $request->input('discussion_id'))->first();

        $abonnes->push($parent);

        foreach ($abonnes as $abonne) {
            $to = User::find($abonne->abonne_id);
            
            if($to->id != $request->input('user_id') && !str_contains($to->email, 'diginote')){
                $from = User::find($request->input('user_id'));
                $data = [
                    'from' => $from->name.' '.$from->lname,
                    'to' => $to->name.' '.$to->lname,
                    'content' => $request->input('content'),
                    'lien' => route('getDiscussions', $request->input('discussion_id'))
                ];
                $to->notify(new CommentNotify($data));
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------------------
    //--- HELPER --------------------------------------------------------------------------------------------------
    private function noteValidator(array $data){

        return Validator::make($data, [
            'nom' => 'required|string',
            'cour_id' => 'required|numeric',
            'u_from' => 'required|numeric',
            'u_to' => 'required|numeric',
            'note' => 'required|numeric',
        ]);
    }
}
