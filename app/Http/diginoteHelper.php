<?php
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Cour;

//-- fmsg
function f_msg($msg){
    return "<div style='background:#fd6868; color:#fff; padding:20px; text-align:center; font-size: 18px; width: 480px; margin: 40px auto; border-radius:4px;'>".$msg." <br> <a href=' ". URL::to('/') . "'>Retour</a> </div>";
}

//-- hasimg
function storeAvatar($avatar, $uid){
    if($avatar){
        $ext = pathinfo($_FILES['avatar']['name'])['extension'];

        if( (!in_array($ext, array('jpg', 'jpeg', 'png', 'svg'))) || ($_FILES['avatar']['size'] > 1000000) || ($_FILES['avatar']['error'] > 0) ){
            return ['error' => true,
                    'ret' => "Erreur dans le type ou la taille du fichier ! (.jpg, .jpeg, .png, .svg) max-file-size: 1M" ];        
        }

        $path = $avatar->storeAs(
            'public/avatars', 'user-'.$uid.'.'.$ext
        );
        return ['error' => false, 'ret' => 'user-'.$uid.'.'.$ext]; 
        //Ex $path = $request->file('avatar')->store('avatars'); //By default, the store method will generate a unique ID to serve as the file name
    }
    return null;
}

//-- getAvatar (get given user avatar)
function getAvatar($uid){
    $profile = User::find($uid)->profile;
    // MayOptimize: check if file exist before return
    ////Append a query string with an arbitrary unique number to force browser refresh img
    return ($profile->hasimg) ? asset('storage/avatars/'.$profile->hasimg.'?x='.rand(0, 999)) : asset('storage/avatars/user-default.svg');
}

//-- get cours title
function getCoursName($id){
    $cour = Cour::find($id);
    return "<b>".$cour->code."</b> ".$cour->title;
}

//-- getMoyenne
function getMoyenne($notes){
    $tot = 0;
    if(count($notes) == 0) return 'nd';
    foreach ($notes as $note) $tot += $note->note;
    return round($tot/count($notes), 2);
}

//-- getAuthRole
function getAuthRole(){
    return Auth::user()->role;
}

//-- getUsername
function getUsername($id){
    $user = User::find($id);
    return $user ?  $user->name.' '.$user->lname : false;
}