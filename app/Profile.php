<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model{
    protected $fillable = [
        'user_id', 'matricule', 'birthday', 'sexe', 'infos', 'role', 'hasimg',
    ];

    //--User relation
	public function user(){
		return $this->belongsTo('App\User');
	}

	//--Classe relation
	public function classe(){
		return $this->belongsTo('App\Classe');
	}

}
