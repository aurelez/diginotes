<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model{
    protected $fillable = [
        'user_id', 'classe_id', 'annee', 'moyenne',
    ];

    //-- User relation    
    public function user(){
        return $this->belongsTo('App\User');
    }

    //-- Note relation
    public function note(){
        return $this->hasMany('App\Note');
    }

    //-- Classe relation    
    public function classe(){
        return $this->belongsTo('App\Classe');
    }
}
