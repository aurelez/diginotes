<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cour extends Model
{
    protected $fillable = [
        'classe_id', 'code', 'title', 'description',
    ];

    // relation classe
    public function post(){
        return $this->belongsTo('App\Classe');
    }

    //-- Note relation
    public function note(){
        return $this->hasMany('App\Note');
    }
}