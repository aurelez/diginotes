<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model{

    protected $fillable = [
        'user_id', 'discussion_id', 'content',
    ];

    // Discussion note
    public function discussion(){
        return $this->belongsTo('App\Discussion');
    }
}
