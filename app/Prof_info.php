<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prof_info extends Model{
    
    protected $fillable = [
        'user_id', 'classe_id', 'cour_id',
    ];

	//--User relation
	public function user(){
		return $this->belongsTo('App\User');
	}
}