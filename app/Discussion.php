<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model{

    protected $fillable = [
        'user_id', 'nom',
    ];

    // relation note
    public function note(){
        return $this->belongsTo('App\Note');
    }

    //-- Note comments
    public function comment(){
        return $this->hasMany('App\Comment');
    }
}
