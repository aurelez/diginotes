<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lname', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //-- Profile relation
    public function profile(){
        return $this->hasOne('App\Profile');
    }

    //--Bulletin relation
    public function bulletin(){
        return $this->hasMany('App\Bulletin')->orderBy('annee');
    }

    //--Prof_info relation
    public function prof_info(){
        return $this->hasMany('App\Prof_info');
    }
}
