<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    protected $fillable = [
        'code', 'nom', 'capacite', 'frais_scolaire',
    ];

    //-- relation cour
    public function cour(){
        return $this->hasMany('App\Cour');
    }

    //-- relation profile
    public function profile(){
        return $this->hasMany('App\Profile');
    }
    
    //-- relation bulletin
    public function bulletin(){
        return $this->hasMany('App\Bulletin');
    }
}
