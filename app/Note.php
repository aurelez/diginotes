<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model{
    protected $fillable = [
        'nom', 'cour_id', 'u_from', 'u_to', 'bulletin_id',  'discussion_id', 'note',
    ];

    //--Cour relation
	public function cour(){
		return $this->belongsTo('App\Cour');
	}

	//--Bulletin relation
	public function bulletin(){
		return $this->belongsTo('App\Bulletin');
	}

    //--Discussion relation
    public function discussion(){
        return $this->hasOne('App\Discussion');
    }
}
