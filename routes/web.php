<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//-- Racine
Route::get('/', function(){return redirect('/home');});

//-- Home
Route::get('home', 'HomeController@index')->name('home');

//-- Auth
Auth::routes();

//-- about
Route::get('about', function() { return 'DigiNote! Portail collegial'; });

//-- Other auth Routes
Route::middleware(['auth'])->group(function () {
    //----PROFILE
    //--------------------
    Route::get('profile', 'ProfileController@profile')->name('userProfile');
    Route::post('profile', 'ProfileController@updateProfile')->name('updateProfile');

    //----DISCUSSION
    //--------------------
    Route::get('discussions', 'DiscussionController@discussions')->name('getDiscussions');

    //----BULLETINS
    //--------------------
    Route::get('bulletins', 'BulletinController@bulletins')->name('getBulletins');
    Route::get('enfantsbulletins', 'BulletinController@enfantsBulletins')->name('getEnfantsBulletins');
});

//-- Admin Routes
Route::prefix('admin')->middleware(['auth', 'checkadmin'])->namespace('Admin')->group(function () {

	//----ELEVES
    //---------------------------------------------------------------------------------------------
    Route::get('eleves', 'EleveController@eleves')->name('adminEleves');

  	Route::post('eleves', 'EleveController@postEleve')->name('adminPostEleve');

    Route::get('eleve/{uid}', 'EleveController@eleve', function($uid){
    })->where('uid','[0-9]+')->name('adminEleve');

    Route::post('eleve', 'EleveController@updateEleve')->name('adminUpdateEleve');

    Route::post('bulletin', 'EleveController@postBulletin')->name('adminPostBulletin');

    //---COURS
    //---------------------------------------------------------------------------------------------
    Route::get('cours', 'CourController@cours')->name('adminCours');

    Route::post('cours', 'CourController@postCour')->name('adminPostCour');

	Route::get('cour/{id}', 'CourController@cour', function($id){
	})->where('id','[0-9]+')->name('adminCour');

    Route::post('cour', 'CourController@updateCour')->name('adminUpdateCour');

    //----CLASSES
    //---------------------------------------------------------------------------------------------
    Route::get('classes', 'ClasseController@classes')->name('adminClasses');

    Route::post('classes', 'ClasseController@postClasse')->name('adminPostClasse');

	Route::get('classe/{id}', 'ClasseController@classe', function($id){
	})->where('id','[0-9]+')->name('adminClasse');

    Route::post('classe', 'ClasseController@updateClasse')->name('adminUpdateClasse');

    //----PARENTS
    //---------------------------------------------------------------------------------------------
    Route::get('parents', 'ParentController@parents')->name('adminParents');

    Route::post('parents', 'ParentController@postParent')->name('adminPostParent');

    Route::get('parent/{id}', 'ParentController@parent', function($uid){
    })->where('id','[0-9]+')->name('adminParent');

    Route::post('parent', 'ParentController@updateParent')->name('adminUpdateParent');

    //----PROFESSEUR
    //---------------------------------------------------------------------------------------------
    Route::get('professeur', 'ProfesseurController@professeurs')->name('adminProfesseurs');

    Route::post('professeurs', 'ProfesseurController@postProfesseur')->name('adminPostProfesseur');

    Route::get('professeur/{id}', 'ProfesseurController@professeur', function($uid){
    })->where('id','[0-9]+')->name('adminParent');

    Route::post('professeur', 'ProfesseurController@updateProfesseur')->name('adminUpdateProfesseur');

    Route::post('delcour', 'ProfesseurController@rmvProfCour')->name('adminRmvProfCour'); 
});



//---------------------------------------------------------------------------------------------
//-- Profs Routes
//---------------------------------------------------------------------------------------------
Route::prefix('prof')->middleware(['auth', 'checkprof'])->namespace('Prof')->group(function () {

    //---COURS
    //---------------------------------------------------------------------------------------------
    Route::get('cours', 'CourController@cours')->name('profCours');

    Route::get('classe/{classe_id}/cour/{cour_id}', 'CourController@cour', function($classe_id, $cour_id){
    })->where(['classe_id' => '[0-9]+', 'cour_id' => '[0-9]+'])->name('profCour');
});


//---------------------------------------------------------------------------------------------
//-- Parent Routes
//---------------------------------------------------------------------------------------------
Route::prefix('parent')->middleware(['auth', 'checkparent'])->namespace('Parent')->group(function () {

    //---elevesSuivis
    //---------------------------------------------------------------------------------------------
    Route::get('elevessuivis', 'ParentController@elevesSuivis')->name('getElevesSuivis');
});