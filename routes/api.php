<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//-- BEFORE DEPLOY: Use Auth passport check here https://laracasts.com/series/whats-new-in-laravel-5-3/episodes/13

//-- getUsers
Route::get('getEleves', 'DigiapiController@getEleves')->name('getEleves');

//-- get notes
Route::get('getNotes', 'DigiapiController@getNotes')->name('getNotes');

//-- post note
Route::post('postNote', 'DigiapiController@postNote')->name('postNote');

//-- delete note
Route::delete('delNote', 'DigiapiController@delNote')->name('delNote');

//-- get comments
Route::get('getComments', 'DigiapiController@getComments')->name('getComments');

//-- post comment
Route::post('postComment', 'DigiapiController@postComment')->name('postComment');

