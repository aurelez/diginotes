<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserActivation;

use JavaScript;
use App\User;
use App\Classe;
use App\Cour;
use App\Bulletin;

use AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class AdminController extends Controller
{
	use RegistersUsers;
    use Notifiable;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    //-- eleves list: Note: the list is geted by api from axios for the live search. see app/Http/digiapiController.php)
    //---------------------------------------------------------------------------------------------------------------------
    public function eleves(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'eleves',
            'avatar' => getAvatar(Auth::user()->id),
            'classes' => Classe::all(),
            'parents' => DB::table('users')->select('id', 'name', 'lname')->where('role', 'parent')->get(),
        ];

        return view('admin/eleves', ['data' => $data]);
    }

    //-- postEleve (adding eleve to DB by admin)
    //---------------------------------------------------------------------------------------------------------------------
    public function postEleve(Request $request){
    	// adding user

    	$regRoute = 'App\Http\Controllers\Auth\RegisterController';
    	$data = array_add($request->input(), 'role', 'eleve');
    	$validate = app($regRoute)->Validator( $data );

        //-- validate
    	if($validate->fails()){
    		return redirect()->back()->withInput()->withErrors($validate->errors());
    	}

    	$user = app($regRoute)->create( $data );

    	//update user profile
    	$user->profile->sexe = $request->input('sexe');
    	if($request->input('classe_id'))
    		$user->profile->classe_id = $request->input('classe_id');
    	if($request->input('parent_id'))
    		$user->profile->parent_id = $request->input('parent_id');

    	//--active the user if needed
    	if($request->input('active'))
    		$user->active = 1;

    	$user->profile->save();
    	$user->save();

    	//dd($user, $user->profile);

    	return redirect()->back()->with([
                'status' => "<b>".$user->lname . ' ' . $user->name . "</b> a bien été inscrit!",
            ]);
    }

    //-- single eleve (Show single eleve infos)
    //---------------------------------------------------------------------------------------------------------------------
    public function eleve($uid){

        $user = User::find($uid);
        if(!$user || $user->role != 'eleve')
            return f_msg("Il n'existe aucun élève correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'eleves',
            'avatar' => getAvatar(Auth::user()->id),
            'user' => $user,
            'parent' => User::find( $user->profile->parent_id ),
            'classes' => Classe::all(),
            'parents' => DB::table('users')->select('id', 'name', 'lname')->where('role', 'parent')->get(),
        ];
        return view('admin/eleve', ['data' => $data]);
    }

    //-- updateEleve (uptade a eleve info + adding photo)
    //---------------------------------------------------------------------------------------------------------------------
    public function updateEleve(Request $request){

        $data = $request->input();
        $user = User::find($data['uid']);

        //-- check if email is changed
        if( $user->email != $data['email'] ){
            $validate = $this->userValidator($data, 'email');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        if( strlen($request->input('password')) > 0 || strlen($request->input('password_confirmation')) > 0 ){
            $validate = $this->userValidator($data, 'password');
            if($validate->fails())
                return redirect()->back()->withInput()->withErrors($validate->errors());
        }

        //-- check others fields
        $validate = $this->userValidator($data, 'others');
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());


        //-- now just update the eleve infos
        $user->name = $data['name'];
        $user->lname = $data['lname'];
        $user->email = $data['email'];
        if( !empty($request->input('password')) ) $user->password = bcrypt($data['password']);
        $user->profile->birthday = $data['birthday'];
        if($request->input('classe_id')) $user->profile->classe_id = $data['classe_id'];
        if($request->input('parent_id')) $user->profile->parent_id = $data['parent_id'];
        if($request->input('sexe')) $user->profile->sexe = $data['sexe'];

        if($request->file('avatar')){
            $file = storeAvatar($request->file('avatar'), $data['uid']);
            if($file['error']){
                return redirect()->back()->withInput()->withErrors($validate->errors()->add('avatar', $file['ret']));
            }
            $user->profile->hasimg = $file['ret'];
        }

        //-- update eleve satus
        if($request->input('active')){
            $user->active = 1;
            //send notification
            $user->notify(new UserActivation($data));
        }else{$user->active = 0;}

        //-- and save them
        $user->profile->save();
        $user->save();

        //dd($data, $user, $user->profile);

        return redirect()->back()->with([
                'status' => "<b>".$user->profile->matricule."</b> a été mise à jour!"
            ]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Cours list
    //---------------------------------------------------------------------------------------------------------------------
    public function cours(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'cours',
            'avatar' => getAvatar(Auth::user()->id),
            'classes' => Classe::all()
        ];

        JavaScript::put([ //https://github.com/laracasts/PHP-Vars-To-Js-Transformer
            'cours_fromBk' => Cour::all()
        ]);

        return view('admin/cours', ['data' => $data]);
    }

    //-- Add cour
    //---------------------------------------------------------------------------------------------------------------------
    public function postCour(Request $request){

        $data = $request->input();
        $validate = $this->courValidator($data);
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());

        $cour = Cour::create([
            'classe_id' => $data['classe_id'],
            'code' => $data['code'],
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return redirect()->back()->with([
	        'status' => "Le cours <b>".$cour->code."</b> a été bien ajouté!"
	    ]);
    }

    //-- single cour
    //---------------------------------------------------------------------------------------------------------------------
    public function cour($id){

        $cour = Cour::find($id);
        if(!$cour)
            return f_msg("Il n'existe aucun cours correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'cours',
            'avatar' => getAvatar(Auth::user()->id),
            'cour' => $cour,
            'classes' => Classe::all(),
        ];
        return view('admin/cour', ['data' => $data]);
    }

    //-- Update cour
    //---------------------------------------------------------------------------------------------------------------------
    public function updateCour(Request $request){

		$data = $request->input();
	    $cour = Cour::find($data['id']);

	    //-- check if code is changed
	    if( $cour->code != $data['code'] ){
	        $validate = Validator::make($data, ['code' => 'required|string|min:3|max:255|unique:cours']);
	        if($validate->fails())
	            return redirect()->back()->withInput()->withErrors($validate->errors());
	    }

	    $cour->code = $data['code'];
	    $cour->title = $data['title'];
	    $cour->classe_id = $data['classe_id'];
	    $cour->description = $data['description'];

	    $cour->save();

        return redirect()->back()->with([
	        	'status' => "Le cours <b>".$cour->code."</b> a été bien modifié!"
            ]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Classe list
    //---------------------------------------------------------------------------------------------------------------------
    public function classes(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'classes',
            'avatar' => getAvatar(Auth::user()->id),
        ];

        JavaScript::put([
            'classes_fromBk' => Classe::all()
        ]);

        return view('admin/classes', ['data' => $data]);
    }

    //-- Add Classe
    //---------------------------------------------------------------------------------------------------------------------
    public function postClasse(Request $request){

        $data = $request->input();
        $validate = $this->classeValidator($data);
        if( $validate->fails() )
            return redirect()->back()->withInput()->withErrors($validate->errors());

        $classe = Classe::create([
            'code' => $data['code'],
            'nom' => $data['nom'],
            'capacite' => $data['capacite'],
            'frais_scolaire' => $data['frais_scolaire'],
        ]);


        return redirect()->back()->with([
	        'status' => "La classe <b>".$classe->code."</b> a été bien ajouté!"
	    ]);
    }

    //-- single Classe
    //---------------------------------------------------------------------------------------------------------------------
    public function classe($id){

        $classe = Classe::find($id);
        if(!$classe)
            return f_msg("Il n'existe aucune classe correspondant à votre demande!");
        
        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'classes',
            'avatar' => getAvatar(Auth::user()->id),
            'classe' => $classe,
            'eleves' => DB::table('users')->select('users.id', 'name', 'lname')
					    				->join('profiles', 'users.id', '=', 'profiles.user_id')
					    				->where('classe_id', $id)->get(),
        ];
        return view('admin/classe', ['data' => $data]);
    }

    //-- Update Classe
    //---------------------------------------------------------------------------------------------------------------------
    public function updateClasse(Request $request){

		$data = $request->input();
	    $classe = Classe::find($data['id']);

	    //-- check if code is changed
	    if( $classe->code != $data['code'] ){
	        $validate = Validator::make($data, ['code' => 'required|string|min:3|max:255|unique:classes']);
	        if($validate->fails())
	            return redirect()->back()->withInput()->withErrors($validate->errors());
	    }

	    $classe->code = $data['code'];
	    $classe->nom = $data['nom'];
	    $classe->capacite = $data['capacite'];
	    $classe->frais_scolaire = $data['frais_scolaire'];

	    $classe->save();

        return redirect()->back()->with([
	        	'status' => "La classe <b>".$classe->code."</b> a été bien modifié!"
            ]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Add Bulletin
    //---------------------------------------------------------------------------------------------------------------------
    public function postBulletin(Request $request){
		$data = $request->input();
		//dd($data);

        $validate = $this->bulletinValidator($data);
        if($validate->fails())
        	return redirect()->back()->withInput()->withErrors($validate->errors());

        //check if bulletin exist with the same year
        $temp_bul = DB::table('bulletins')->where('user_id', $data['user_id'])->where('annee', $data['annee'])->first();
        if($temp_bul){ 
            return redirect()->back()->withInput()
            ->withErrors($validate->errors()->add('unique_annee', "L'élève a déjà un bulletin pour ".$data['annee']));
        }

        $bulletin = Bulletin::create([
		    'user_id' => $data['user_id'],
		    'classe_id' => $data['classe_id'],
		    'annee' => $data['annee'],
		]);

        return redirect()->back()->with([
	    	'status_bulletin' => "Le bulletin a été ajouté!"
	    ]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Parents list
    //---------------------------------------------------------------------------------------------------------------------
    public function parents(){

        $data = [
            'title' => "DigiNote Admin",
            'activepage' => 'parents',
            'avatar' => getAvatar(Auth::user()->id),
            'classes' => Classe::all()
        ];

        JavaScript::put([ //https://github.com/laracasts/PHP-Vars-To-Js-Transformer
            'cours_fromBk' => Cour::all()
        ]);

        return view('admin/cours', ['data' => $data]);
    }

    //---------------------------------------------------------------------------------------------------------------------
    //-- Helpers
    //---------------------------------------------------------------------------------------------------------------------
    //---Conditional user userValidator
    private function userValidator(array $data, $target){
    
        if($target === 'email')
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
            ]);

        if($target === 'password')
            return Validator::make($data, [
                'password' => 'required|string|min:3|confirmed',
            ]);

        if($target === 'others')
            return Validator::make($data, [
                'name' => 'required|string|min:3|max:255',
                'lname' => 'required|string|min:3|max:255',
            ]);
    }

    //---courValidator
    private function courValidator(array $data){

        return Validator::make($data, [
            'code' => 'required|string|min:3|max:255|unique:cours',
            'title' => 'required|string|min:3|max:255',
            'classe_id' => 'required',
        ]);
    }

    //---classeValidator
    private function classeValidator(array $data){

        return Validator::make($data, [
            'code' => 'required|string|min:3|max:255|unique:classes',
            'nom' => 'required|string|min:3|max:255',
            'capacite' => 'required|numeric',
            'frais_scolaire' => 'required|numeric',
        ]);
    }

    //---bulletinValidator
    private function bulletinValidator(array $data){

        return Validator::make($data, [
            'user_id' => 'required|numeric',
            'classe_id' => 'required|numeric',
            'annee' => 'required|numeric',
        ]);
    }
}
