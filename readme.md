<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](http://vehikl.com)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Styde](https://styde.net)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## DigiNote
- composer dump-autoload php artisan storage:link php artisan passport:install
- php artisan route:list
- https://www.youtube.com/watch?v=uJUJqn7V8pk&list=PLjwdMgw5TTLUCpXVEehCHs99N7IWByS3i&index=7
- Laravel debugbar - gitHub
- http://laravel-recipes.com/
- https://laravelcollective.com/docs/5.2/html
- https://laravel.com/docs/5.4/authentication
- https://stackoverflow.com/questions/3 1015606/login-only-if-user-is-active-using-laravel/31016210#31016210
- https://stackoverflow.com/questions/39813314/calling-function-from-controller-in-view-laravel
- https://learninglaravel.net/topics/vuejs
- https://www.youtube.com/watch?v=BuWLgY_pU_s
- https://www.youtube.com/watch?v=GsPEDI9lZQ4
- https://stackoverflow.com/questions/15285892/laravel-query-builder-with-and-in-query
- REST API with authetification (laravel passeport) https://stackoverflow.com/questions/43576839/laravel-5-4-api-route-list http://esbenp.github.io/2017/03/19/modern-rest-api-laravel-part-4/ https://www.youtube.com/watch?v=j2B5EfMvExU https://gistlog.co/JacobBennett/090369fbab0b31130b51
- storage https://stackoverflow.com/questions/41630393/ https://laracasts.com/series/whats-new-in-laravel-5-3/episodes/13undefined-variable-request
- npm install npm run dev --- npm run production --- npm run watch
- Note: Bon! Laravel 5.4 utilise laravel-mix pour compiler sass et js (comme Gulp ou Grunt) Dans les precedente version il utilisait Laravel-Elixir. 
le fichier webpack.js a la racine contien les regle de Mix pour comiler les assest from ressorce/assets to public/ (faire un choix entre config.rb pour compiler sass ou utiliser Laravel Mix)
- Huge Mysql expired password resolution : http://www.ryadel.com/en/mysql-user-password-expired-permanently-fix/   just add skip-grant-tables and default_password_lifetime=0 to mysql ini file
- force pull https://stackoverflow.com/questions/1125968/how-do-i-force-git-pull-to-overwrite-local-files
- 
- PHP-Vars-To-Js-Transformer https://github.com/laracasts/PHP-Vars-To-Js-Transformer: 
-- add "laracasts/utilities": "~2.0" in require composer 
-- add Laracasts\Utilities\JavaScript\JavaScriptServiceProvider::class in providers im config/app.php 
-- php artisan vendor:publish , this will creat the file javascript.php in config folder, Indicate in this file wich view to use ex: 'bind_js_vars_to_this_view' => 'footer'
-- then add use JavaScript; in controller and add @include ('footer') in your view
-- utilisation: JavaScript::put(['user' => User::first()]);
- FackeSendMail https://www.grafikart.fr/blog/mail-local-wamp

## Todo
- crer une table intermediare prof_cours qui feras la relation entre un prof et les cours qu'il enseigne: idProf et idCours Ex: 1->2, 1->3